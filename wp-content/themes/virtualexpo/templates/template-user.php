<?php
/**
 * Template Name: List User Template
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage aeg_media
 * @since Aeg Media 1.0
 */
get_template_part( 'list-user' );
