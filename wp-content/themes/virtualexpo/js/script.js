window.addEventListener("load", function (event) {

    function getlocalStorageCart() {
        if (localStorage.getItem('addToCart') !== null)
        {
            var sessionCart = localStorage.getItem('addToCart');
            return JSON.parse(sessionCart)
        }
        return null;
    }

    function renderHTMLCountProducts(numberProducts) {
        var contentHTML = `<span class="cd-cart__count">${numberProducts}</span>`;
        jQuery('#nav-ft').find('a[data-target="#briefcase"]').append(contentHTML);
    }

    function notification(idElement,  text)
    {
        $(`#${idElement}>strong`).text(text);
        $(`#${idElement}`).fadeTo(2000, 500).slideUp(500, function() {
            $(`#${idElement}>strong`).text('');
            $(`#${idElement}`).slideUp(500);
        });
    }

    function callAPIUseMethod(objectSessionTemp = null, action) {
        switch (action) {
            case "update":
                $.ajax({
                    type: 'POST',
                    url: ObjectCommon.siteUrl + '/wp-json/exhibition/v1/updateSessionTemp',
                    data: JSON.stringify(objectSessionTemp),
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (data, status, xhr) {
                    if (data)
                    {
                        notification('buttonAlert','Document is added successfully!');
                    }
                }).fail(function (xhr, status, error) {
                    console.debug(error);
                });
                break;
            case 'insert':
                $.ajax({
                    type: 'POST',
                    url: ObjectCommon.siteUrl + '/wp-json/exhibition/v1/insertSessionTemp',
                    data: JSON.stringify(objectSessionTemp),
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (data, status, xhr) {
                    if (data)
                    {
                        notification('buttonAlert','Document is added successfully!')
                    }
                }).fail(function (xhr, status, error) {
                    console.debug(error);
                });
                break;
            case "delete":
                $.ajax({
                    type: 'POST',
                    url: ObjectCommon.siteUrl + '/wp-json/exhibition/v1/updateSessionTemp',
                    data: JSON.stringify(objectSessionTemp),
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function (data, status, xhr) {
                    if (data)
                    {
                        notification('buttonAlertDelete','Document is deleted successfully!');
                    }
                }).fail(function (xhr, status, error) {
                    console.debug(error);
                });
                break;
            default:
        }
    }


     function countProducts()
    {
        if (getlocalStorageCart())
        {
            var numberProducts = getlocalStorageCart();
            return renderHTMLCountProducts(numberProducts.product.length);

        }
        $.ajax({
            type: 'GET',
            url: ObjectCommon.siteUrl + '/wp-json/exhibition/v1/getSessionTemp?userId=' + ObjectCommon.userId,
            dataType: 'json',
            processData: false,
            contentType: false,
            error: function(xhr) {
                console.debug(xhr);
            }
        }).done(function (data, status, xhr) {
            if (data.length > 0)
            {
                var objectSessionTemp = {
                    userId : data[0].userId,
                    product : JSON.parse(data[0].products)
                }
                localStorage.addToCart = JSON.stringify(objectSessionTemp);
                var numberProducts = getlocalStorageCart();
                renderHTMLCountProducts(numberProducts.product.length);
            }
        });
    }
    countProducts();

    function checkWidth() {
        let w = jQuery('.element__all').innerWidth();
        var font = w / 100;
        jQuery(".header__topbar2").css({
            'font-size': font,
        });
        jQuery(".element__all").css({
            'font-size': font,
        });
    }
    checkWidth();
    jQuery(window).resize(function () {
        checkWidth();
    });
    function checkWidth2() {
        let w2 = jQuery('body').innerWidth();
        var font2 = w2 / 100;
        jQuery(".banner").css({
            'font-size': font2,
        });
    };
    checkWidth2();
    jQuery(window).resize(function () {
        checkWidth2();
    });

    jQuery(".slider-product--tem1.owl-carousel").owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        autoplay:true,
        autoplayTimeout:5000,
        nav: true,
        dots: false,
        navText: ["‹", "›"],
    });
    jQuery(".slider-product--tem3.owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        items: 2,
        autoplay:true,
        autoplayTimeout:5000,
        nav: true,
        dots: false,
        navText: ["‹", "›"],
    });


    jQuery(".category__main1--list").hover(function () {
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass("active");
            jQuery(".category__main1--list ul").slideUp();
        } else {
            jQuery(this).addClass("active");
            jQuery(this).children(".category__main1--list ul").slideDown();
        }
    });
    jQuery("#menu-click").click(function (event) {
        let offsetNavTop = $(this).offset().top - 147;
        let offsetNavLeft = $(this).offset().left + 10;
        jQuery(".menu-navigation").css({"top": offsetNavTop, "left": offsetNavLeft});
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass("active");
            jQuery(".menu-navigation").removeClass('open');
        } else {
            jQuery(".menu-navigation").addClass('open');
            jQuery(this).addClass("active");
        }
        event.stopPropagation();
    });
    jQuery('body').on('click', function () {
        if (jQuery('#menu-click').hasClass("active")) {
            jQuery('#menu-click').removeClass("active");
            jQuery(".menu-navigation").removeClass('open');
        }
    });
    jQuery('.dot-zoom').on('click', function () {
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass("active");
            jQuery(".divFull").removeClass('open');
        } else {
            jQuery(this).addClass("active");
            jQuery(".divFull").addClass('open');
        }
    });



    jQuery(".profile-header>a").click(function (event) {
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass("active");
            jQuery(".profile-header>ul").hide();
        } else {
            jQuery(".profile-header>ul").show();
            jQuery(this).addClass("active");
        }
        event.stopPropagation();
    });
    jQuery('body').on('click', function(){
        if(jQuery(".profile-header>a").hasClass("active")){
            jQuery(".profile-header>a").removeClass("active");
            jQuery(".profile-header>ul").hide();
        }
    });

    var urlLogour = ObjectCommon.siteUrl + '/logout/';
    jQuery("a[href='"+urlLogour+"']").on('click', function () {
        localStorage.removeItem('addToCart')
    })

    jQuery('#add-to-cart').on('click', function () {

        var sessionCart = getlocalStorageCart(); // get localStorage cart
        var documentLink = $('a#document-link').attr('href'); // get Link document
        var documentName = $('div#document-name').text();
        var booth  = document.title; // get title booth
        var boothLink = window.location.href;
        var product = [{booth, boothLink, documentName, documentLink}];

        if (sessionCart)
        {
            for(var i = 0; i < sessionCart.product.length; i++)
            {
                if (sessionCart.product[i].documentLink === documentLink)
                {
                    notification('buttonAlert','This document is available in Briefcase')
                    return countProducts();
                }
            }
            sessionCart.product.push({booth, boothLink, documentName, documentLink})
            localStorage.addToCart = JSON.stringify(sessionCart);

            var data = getlocalStorageCart();
            var objectSessionTemp = {
                userId : ObjectCommon.userId,
                product : data.product
            };

            countProducts();
            return callAPIUseMethod(objectSessionTemp,'update');
        }

        var objectSessionTemp = {
            userId :  ObjectCommon.userId,
            product : product
        };
        localStorage.addToCart = JSON.stringify(objectSessionTemp);
        countProducts();

        return callAPIUseMethod(objectSessionTemp,'insert');
    });

    $("#wrapTableBriefCase").on("click", "button", function() {
        if (confirm('Are you sure you want to remove this from the shopping cart?'))
        {
            var sessionCart = getlocalStorageCart(); // get localStorage cart
            var indexElement = $(this).closest('tr').index();
            sessionCart.product.splice(indexElement,  1);
            var objectSessionTemp = {
                userId : sessionCart.userId,
                product : sessionCart.product
            };
            localStorage.addToCart = JSON.stringify(sessionCart);
            countProducts();
            $("#tableBriefCase>tbody").children()[indexElement].remove();
            return callAPIUseMethod(objectSessionTemp,  'delete')
        }
    });
});



