jQuery(document).ready(function ($) {
    'use strict';
    let urlBase = window.location.origin;
    urlBase = urlBase;
    let svg = d3.select("#wrap_admin").append("svg");
    let svgnew = d3.select("#wrap_admin svg");
    let count = 0;
    let drag = d3.drag()
        .on("start", onDragStart)
        .on("drag", dragged)
        .on("end", dragend);
    let nodes_data =
        [{
            x: 0,
            y: 0
        }];
    /*let svg = d3.select("#wrap_admin svg");*/
    let container = d3.select("#wrap_admin svg g");
    let searchParams = new URLSearchParams(window.location.search);
    let idElement;
    let arrSvg = [];
    let element = [];
    let zoom = d3.zoom()
        .scaleExtent([1, 1])
        .translateExtent([[0, 0], [3200, 1744]])
        .on('zoom', zoomed);
    svg.call(zoom).on("wheel", exitZoom);
    $.ajax({
        type: 'GET',
        url: urlBase + '/wp-json/exhibition/v1/getBackground?idPage=' + searchParams.get('id'),
        dataType: 'json',
        processData: false,
        contentType: false
    }).done(function (data, status, xhr) {
        if (data != '') {
            let dataArray = data;
            container = svg.append("g").attr("id","background-g");
            let mapImage = container.append("image")
                .attr("xlink:href", dataArray[0].background)
                .attr("id","background-svg")
                .attr("width", dataArray[0].width);
        }
        $.ajax({
            type: 'GET',
            url: urlBase + '/wp-json/exhibition/v1/getJson?idPage=' + searchParams.get('id'),
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function (data, status, xhr) {
            if (data != '') {
                let dataArray = data;
                let container = d3.select("#wrap_admin svg g");
                for (let i = 0; i < dataArray.length; i++) {
                    let transformOrigin = [];
                    if (dataArray[i].transform) {
                        let transform = dataArray[i].transform.replace('translate(', "")
                        transform = transform.replace(')', "")
                        transformOrigin = transform.split(',')
                    } else {
                        transformOrigin.push(0, 0);
                    }
                    let nodes_data_new =
                        [{
                            x: transformOrigin[0],
                            y: transformOrigin[1]
                        }];
                    let rectangle = container.append("image")
                        .attr("id", dataArray[i].id)
                        .attr("xlink:href", dataArray[i].urlLogo)
                        .attr("x", 67)
                        .attr("y", 4)
                        .attr("transform", dataArray[i].transform)
                        .classed('draggable', true);
                    rectangle.call(drag).data(nodes_data_new);
                    rectangle.call(drag).on('click', function () {
                        jQuery("#confirm-edit").addClass('show');
                        idElement = this.getAttribute('id')
                    })
                    let local = {
                        "id": dataArray[i].id,
                        "nameCompany": dataArray[i].nameCompany,
                        "linkCompany": dataArray[i].linkCompany,
                        "urlLogo": dataArray[i].urlLogo,
                        "urlLogoBig": dataArray[i].urlLogoBig,
                        "transform": dataArray[i].transform,
                        "description": dataArray[i].description
                    };
                    arrSvg.push(local);
                }
            }
        }).fail(function (xhr, status, error) {
            console.debug(error);
        });
    }).fail(function (xhr, status, error) {
        console.debug(error);
    });
    $("#clickAddElement").on('click', function (){
        $("#wrap-show").addClass('show');
    })
    $("#delete").on('click', function () {
        $('#confirm-edit').removeClass('show');
        let container = d3.select("#wrap_admin svg g");
        arrSvg.forEach((type, index) => {
            if (type.id === idElement) {
                arrSvg.splice(index, 1);
            }
        })
        let container_remove = d3.selectAll(".draggable").remove();
        for (let i = 0; i < arrSvg.length; i++) {
            let rectangle = container.append("image")
                .attr("id", arrSvg[i].id)
                .attr("xlink:href", arrSvg[i].urlLogo)
                .attr("x", 67)
                .attr("y", 4)
                .attr("transform", arrSvg[i].transform)
                .classed('draggable', true);
            rectangle.selectAll(".draggable").call(drag).data(nodes_data);
            rectangle.on('click', function () {
                jQuery("#confirm-edit").addClass('show');
                idElement = this.getAttribute('id')
            })
        }
    });
    $("#edit").on('click', function () {
        let svgEdit = [];
        arrSvg.forEach((type, index) => {
            if (type.id === idElement) {
                svgEdit = type;
            }
        })

        let img = '<img src="'+ svgEdit.urlLogo +'" height="60px">'
        if(svgEdit.urlLogo){
            $('#btnUpLoadLogo').removeClass('show');
            $('#remove-image').addClass('show');
        }

        let imgBig = '<img src="'+ svgEdit.urlLogoBig +'" height="60px">'
        if(svgEdit.urlLogoBig){
            $('#btnuploadLogoBig').removeClass('show');
            $('#remove-imageBig').addClass('show');
        }

        $('#idElement').val(svgEdit.id);
        $('#company-name').val(svgEdit.nameCompany);
        $('#description-company').val(svgEdit.description);
        $('#link-company').val(svgEdit.linkCompany);

        // logo company
        $('#logoCompany').val(svgEdit.urlLogo);
        $('#uploadLogo img').remove();
        $('#uploadLogo').append(img);

        // logo company Big
        $('#logoCompanyBig').val(svgEdit.urlLogoBig);
        $('#uploadLogoBig img').remove();
        $('#uploadLogoBig').append(imgBig);

        $('#wrap-show').addClass('show');
        $('#confirm-edit').removeClass('show');
    })
    $("#saveJson").on('click', function () {
        $.ajax({
            type: 'POST',
            url: urlBase + '/wp-json/exhibition/v1/update?id=' + searchParams.get('id'),
            data: JSON.stringify(arrSvg),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (data, status, xhr) {

        }).fail(function (xhr, status, error) {
            console.debug(error);
        });
    });
    $("#sendElement").on('click', function () {
        element = [];
        let idElementEdit = $('#idElement').val()
        let name = $('#company-name').val();
        let linkCompany = $('#link-company').val();
        let descriptionCompany = $('#description-company').val();
        let logoCompany = $('#logoCompany').val();
        let logoCompanyBig = $('#logoCompanyBig').val();
        let local;
        if(idElementEdit){
            local = {
                "id": idElementEdit,
                "nameCompany": name,
                "linkCompany": linkCompany,
                "description": descriptionCompany,
                "urlLogo": logoCompany,
                "urlLogoBig" : logoCompanyBig
            };
        } else {
            let d = new Date();
            let id = d.getTime();
            local = {
                "id": 'id' + id,
                "nameCompany": name,
                "linkCompany": linkCompany,
                "description": descriptionCompany,
                "urlLogo": logoCompany,
                "urlLogoBig" : logoCompanyBig
            };
        }
        element.push(local);
        $.ajax({
            type: 'POST',
            url: urlBase + '/wp-json/exhibition/v1/createElement?id=' + searchParams.get('id'),
            data: JSON.stringify(element),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (data, status, xhr) {
            ajaxSuccess(data, status, xhr ,idElementEdit);
        }).fail(function (xhr, status, error) {
            console.debug(error);
        });
    });
    $("#submitBackground").on('click', function(){
        let widthSvg = $("#width-background").val();
        let heightSvg = $("#height-background").val();
        let urlBackground = $("#urlBackground").val();
        let arrBackground = [];
        let background = {
            "width": widthSvg,
            "height": heightSvg,
            "background": urlBackground
        };
        arrBackground.push(background);
        $.ajax({
            type: 'POST',
            url: urlBase + '/wp-json/exhibition/v1/createBackground?id=' + searchParams.get('id'),
            data: JSON.stringify(arrBackground),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (data, status, xhr) {
            ajaxUploadSuccess(data, status, xhr , background);
            /**/
        }).fail(function (xhr, status, error) {
            console.debug(error);
        });
    })
    let ajaxSuccess = function (data, status, xhr, idElementEdit) {
        switch (data.status) {
            case 'validation_failed':
                $.each(data.invalid_fields, function (i, n) {
                    $(n.into).each(function () {
                        notValidTip(this, n.message);
                        $('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
                    });
                });
                break;
            case 'successfull':
                $("#wrap-show").removeClass('show');
                cleanForm();
                let container = d3.select("#wrap_admin svg g");
                if(idElementEdit){
                    arrSvg.forEach((type , index) => {
                        if(type.id === idElementEdit){
                            type.linkCompany = element[0].linkCompany;
                            type.nameCompany = element[0].nameCompany;
                            type.urlLogo = element[0].urlLogo;
                            type.urlLogoBig = element[0].urlLogoBig;
                            type.description = element[0].description;
                        }
                    });
                    let container_remove = d3.selectAll(".draggable").remove();
                    for (let i = 0; i < arrSvg.length; i++) {
                        let rectangle = container.append("image")
                            .attr("id", arrSvg[i].id)
                            .attr("xlink:href", arrSvg[i].urlLogo)
                            .attr("x", 67)
                            .attr("y", 4)
                            .attr("transform", arrSvg[i].transform)
                            .classed('draggable', true);
                        rectangle.call(drag).data(nodes_data);
                        rectangle.call(drag).on('click', function () {
                            jQuery("#confirm-edit").addClass('show');
                            idElement = this.getAttribute('id')
                        })
                    }
                } else{
                    let rectangle = container.append("image")
                        .attr("id", element[0].id)
                        .attr("xlink:href", element[0].urlLogo)
                        .attr("x", 67)
                        .attr("y", 4)
                        .classed('draggable', true);
                    var local = {"id": element[0].id, "nameCompany": element[0].nameCompany, "linkCompany": element[0].linkCompany, "urlLogo": element[0].urlLogo, "urlLogoBig" : element[0].urlLogoBig, "description": element[0].description};
                    arrSvg.push(local);
                    nodes_data =
                        [{
                            x: 0,
                            y: 0
                        }];
                    rectangle.call(drag).data(nodes_data);
                    rectangle.call(drag).on('click', function () {
                        jQuery("#confirm-edit").addClass('show');
                        idElement = this.getAttribute('id')
                    })
                }
                break;
            default:
                wpcf7.setStatus($form,
                    'custom-' + data.status.replace(/[^0-9a-z]+/i, '-')
                );
        }
    };
    let notValidTip = function (target, message) {
        var $target = $(target);
        $('.wpcf7-not-valid-tip', $target).remove();

        $('<span></span>').attr({
            'class': 'wpcf7-not-valid-tip',
            'role': 'alert',
            'aria-hidden': 'true',
        }).text(message).appendTo($target);

        if ($target.is('.use-floating-validation-tip *')) {
            var fadeOut = function (target) {
                $(target).not(':hidden').animate({
                    opacity: 0
                }, 'fast', function () {
                    $(this).css({'z-index': -100});
                });
            };

            $target.on('mouseover', '.wpcf7-not-valid-tip', function () {
                fadeOut(this);
            });

            $target.on('focus', ':input', function () {
                fadeOut($('.wpcf7-not-valid-tip', $target));
            });
        }
    };
    let ajaxUploadSuccess = function (data, status, xhr, background) {
        switch (data.status) {
            case 'validation_failed':
                jQuery.each(data.invalid_fields, function (i, n) {
                    jQuery(n.into).each(function () {
                        notValidTip(this, n.message);
                        jQuery('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
                    });
                });
                break;
            case 'successfull':
                d3.selectAll("g").remove();
                jQuery("#wrap-show-background").removeClass('show');
                container = svg.append("g").attr("id","background-g")
                    .attr("width", background.width)
                    .attr("height", background.height);
                let mapImage = container.append("image")
                    .attr("xlink:href", background.background)
                    .attr("id","background-svg")
                if(mapImage){
                    jQuery('#saveJson').addClass('show');
                    jQuery("#wrap-show-background").removeClass('show');
                }
                break;
            default:
        }
    };
    let notValidTipNew = function (target, message) {
        var $target = jQuery(target);
        jQuery('.wpcf7-not-valid-tip', $target).remove();
        jQuery('<span></span>').attr({
            'class': 'wpcf7-not-valid-tip',
            'role': 'alert',
            'aria-hidden': 'true',
        }).text(message).appendTo($target);
        if ($target.is('.use-floating-validation-tip *')) {
            var fadeOut = function (target) {
                jQuery(target).not(':hidden').animate({
                    opacity: 0
                }, 'fast', function () {
                    jQuery(this).css({'z-index': -100});
                });
            };
            jQuerytarget.on('mouseover', '.wpcf7-not-valid-tip', function () {
                fadeOut(this);
            });

            jQuerytarget.on('focus', ':input', function () {
                fadeOut(jQuery('.wpcf7-not-valid-tip', $target));
            });
        }
    };
    function cleanForm() {
        $('#company-name').val('');
        $('#link-company').val('');
        $('#description-company').val('');

        // logo
        $('#logoCompany').val('');
        $('#uploadLogo img').remove();
        $('#remove-image').removeClass('show');
        $('#btnUpLoadLogo').addClass('show');

        // logo big
        $('#logoCompanyBig').val('');
        $('#uploadLogoBig img').remove();
        $('#remove-imageBig').removeClass('show');
        $('#btnuploadLogoBig').addClass('show');
    }
    function zoomed(event) {
        container.attr("transform", event.transform);
    }
    function dragend(d) {
        arrSvg.forEach((type , index) => {
            if(type.id === this.getAttribute('id')){
                arrSvg[index].transform = this.getAttribute('transform')
            }
        })
    }
    function onDragStart(d) {

    }
    function dragged(event, d) {
        d.x = event.x;
        d.y = event.y;
        //Translate the object on the actual moved point
        d3.select(this).raise().attr("transform", "translate(" + d.x + "," + d.y + ")");
        count ++;
    }
    function exitZoom(event){
        event.preventDefault()
    }
});
jQuery( document ).ready(function($){
    if($('#page_template').length > 0){
        if($('#page_template').val() == 'templates/template-lobby.php'){
            $('#add-pdf').addClass('show')
        }
        $('#page_template').change(function() {
            console.debug('test', jQuery('#page_template').val());
            if (jQuery('#page_template').val() == 'templates/template-lobby.php'){
                console.debug('test');
                jQuery('#add-pdf').addClass('show')
            } else{
                jQuery('#add-pdf').removeClass('show')
            }
        });
    }
});

