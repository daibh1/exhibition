window.Exc = window.Exc || function (mode, { host, port, transport }, { username, name }, post) {
  this.initial = () => {
    this.initialInterface();
    this.initialSocket();
  };
  this.initialInterface = () => {
    // to format date
    const _dateOptions = { dateStyle: 'short', timeStyle: 'short' };
    const _toDateStr = (date) => {
      date = date || new Date();
      date = typeof date === 'string' ? new Date(date) : date;
      const _format = Intl.DateTimeFormat('en', _dateOptions);
      return _format.format(date);
    };
    this.masterRef = $('#nw-chat-master');
    if (!this.masterRef.length) {
      this.masterRef = $('<div id="nw-chat-master"></div>');
      $(document.body).append(this.masterRef);
    }
    let _baseRef;
    switch (mode) {
      case 'SUBSCRIBER_PAGE':
        // initial wrapper
        _baseRef = $(`
          <div class="container">
            <div class="row">
              <div class="col-4">
                <div class="nw-exhibitions" style="display: flex; flex-direction:column; overflow-y: auto;"></div>
              </div>
              <div class="col-8">
                <div class="nw-exhibition-conversion"></div>
              </div>
            </div>
          </div>
          <div class="container nw-admin-message">
            <div class="row nw-message-wrapper">
              <div class="col-md-3 nw-message-list">
                <div class="row nw-list-header">
                  <div class="nw-list-heading col-3">Recent</div>
                  <div class="nw-list-search col">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search">
                      <div class="input-group-append">
                        <button class="btn" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row nw-list-content nw-list-room toggled"></div>
                <div class="row nw-list-content nw-list-conversion"></div>
              </div>
              <div class="col-md nw-message-content">
                <div class="nw-message-history"></div>
                <div class="col-12 nw-user-typing"></div>
                <div class="col-12 nw-message-reply">
                  <div class="nw-input-group">
                    <input type="text" class="nw-control-chat" placeholder="Type a message" />
                    <button class="nw-button-send" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        `);
        this.masterRef.append(_baseRef);
        this.wrapperRef = () => $(`.nw-message-content`);
        this.exhibitionRef = () => $(`.nw-message-reply`);

        this.conversionRefs = () => $('.nw-list-content.nw-list-conversion');
        this.conversionRef = ({ name, message, id }) => {
          let _convRef = $(
            `<div class="col-12 nw-list-item">
              <div class="row nw-item-wrapper">
                <div class="col-2  nw-item-figure"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="col nw-item-content">
                  <h5>${name ?? ''} <span class="nw-item-sub">Dec 25</span></h5>
                  <p>${message ?? ''}</p>
                </div>
              </div>
            </div>`
          );
          _convRef.data('id', id);
          return _convRef;
        };
        this.conversionHandle = (e) => {
          e.preventDefault();
          let _target = $(e.target);
          _target = _target.hasClass('.nw-list-item') ? _target : _target.closest('.nw-list-item');
          let _id = _target.data('id');
          // do something to join & fetch content of conversion with id {_id}
          this.joinTo({ _id });
        };
        this.messageRefs = () => $('.nw-message-history');
        this.inCommingMessage = ({ message, createdAt }) => $(`
          <div class="row nw-message">
            <div class="col-8 nw-message-incoming">
              <div class="row">
                <div class="col-1 nw-item-figure"> <img src="https://ptetutorials.com/images/user-profile.png"> </div>
                <div class="col nw-item-content">
                  <div class="nw-message-block">
                    <p>${message}</p>
                    <span class="nw-item-sub"> ${_toDateStr(createdAt)} </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        `);
        this.outGoingMessage = ({ message, createdAt }) => $(`
          <div class="row nw-message">
            <div class="offset-4 col nw-message-outgoing">
              <div class="col nw-message-block">
                <p>${message}</p>
                <span class="nw-item-sub"> ${_toDateStr(createdAt)} </span>
              </div>
            </div>
          </div>
        `);
        this.switchConversion = ({ ref, id, sendTo }) => {
          ref.data('id', id);
          ref.off() // off all previous events of reply box
            .on('keypress', '.nw-control-chat', (e) => { // regist key press events
              if (e.which === 13 || e.keyCode === 13) {
                e.preventDefault();
                // call socket to send message to partner
                let _val = $(e.target).val();
                sendTo({ _id: id, message: _val });
              } else {
                // call socket to remind partner you're typing

              }
            })
            .on('click', '.nw-button-send', (e) => { // regist click on send button
              e.preventDefault();
              let _val = $(e).siblings('.nw-control-chat').val();
              sendTo({ _id: id, message: _val });
            });
        };
        this.clearInput = (id) => {
          const _input = $('.nw-control-chat', this.exhibitionRef(id));
          _input.val('');
          _input.focus();
        };
        break;

      case 'SUBSCRIBER_POPUP':
        _baseRef = $(`
          <div id="nw-visitor-chat" class="nw-visitor-chat">
            <div class="nw-visitor-chat-wrapper">
              <div class="nw-exhibition-conversion">
                <div id="profile">
                  <div class="wrap">
                    <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="" />
                    <p class="nw-user-info" style="padding-left: 10px;">
                      <?php echo $current_user->display_name . " - " . $current_user->company_name ?>
                    </p>
                  </div>
                </div>
                <div id="search">
                  <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                  <input type="text" placeholder="Search contacts..." />
                </div>
                <div class="nw-exhibition-list">
                  <ul>
                    <li class="nw-exhibition">
                      <div class="wrap">
                        <span class="contact-status online"></span>
                        <img src="http://emilcarlsson.se/assets/louislitt.png" alt="" />
                        <div class="meta">
                          <p class="name">Louis Litt</p>
                          <p class="preview">You just got LITT up, Mike.</p>
                        </div>
                      </div>
                    </li>
                    <li class="nw-exhibition active">
                      <div class="wrap">
                        <span class="contact-status busy"></span>
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <div class="meta">
                          <p class="name">Harvey Specter</p>
                          <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or,
                            you do any one of a hundred and forty six other things.</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div id="bottom-bar">
                  <button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add
                      contact</span></button>
                  <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
                </div>
              </div>
            </div>
          </div>
        `);
        this.masterRef.append(_baseRef);
        this.chatRef = () => $('#nw-visitor-chat');
        this.wrapperRef = () => $('#nw-visitor-chat > .nw-visitor-chat-wrapper');
        this.exhibitionRef = (id) => $(`.nw-chat-content`, this.wrapperRef()).filter((i, _) => $(_).data('id') == id);

        this.conversionRefs = () => $('.nw-exhibition-conversion > .nw-exhibition-list > ul');
        this.conversionRef = ({ name, message, id }) => {
          let _title = name;
          let _convRef = $(`
            <li class="nw-exhibition">
              <div class="wrap">
                <span class="contact-status online"></span>
                <img src="http://emilcarlsson.se/assets/louislitt.png" alt="" />
                <div class="meta">
                    <p class="name">${_title}</p>
                    <p class="preview">${message ?? ''}</p>
                </div>
                <div class="tooltip bs-tooltip-bottom" role="tooltip">
                  <div class="arrow" style="left: 52px;"></div>
                  <div class="tooltip-inner">${_title}</div>
                </div>
              </div>
            </li>
          `);
          _convRef.on('mouseenter', function () {
            $(".tooltip", $(this)).fadeToggle(400);
          });
          _convRef.on('mouseleave', function () {
            $(".tooltip", $(this)).fadeToggle(400);
          });
          _convRef.data('id', id);
          return _convRef;
        };
        this.conversionHandle = (e) => {
          let _target = $(e.target);
          _target = _target.hasClass('nw-exhibition') ? _target : _target.closest('.nw-exhibition');
          let _id = _target.data('id');
          // do something to join & fetch content of conversion with id {_id}
          this.joinTo({ _id });
        };
        this.messageRefs = (id) => $('.nw-chat-messages > .nw-room-conversion', this.exhibitionRef(id));
        this.inCommingMessage = ({ name, message, createdAt, fromAuthor }) => $(`
          <li class="nw-message-incoming">
            <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
            <div>
              <div>
                <span class="nw-user ${fromAuthor ? 'nw-author' : ''}">${name}</span>
                <span>${_toDateStr(createdAt)}</span>
              </div>
              <p>${message}</p>
            </div>
          </li>
        `);
        this.outGoingMessage = ({ message, createdAt }) => $(`
          <li class="nw-message-outgoing">
            <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
            <div>
              <div>
                <span>${_toDateStr(createdAt)}</span>
              </div>
              <p>${message}</p>
            </div>
          </li>
        `);
        this.newConversionRef = ({ id, name, sendTo }) => {
          const _ref = $(`
            <div class="nw-chat-content active">
              <div class="nw-chat-header">
                <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                <ul class="nw-heading-title">
                    <li>${name}</li>
                </ul>
              </div>
              <div class="nw-chat-messages">
                <ul class="nw-room-conversion"></ul>
              </div>
              <div class="nw-chat-reply">
                <div class="wrap">
                  <input id="input" class="nw-visitor-control-chat" type="text" autocomplete="off" autofocus="autofocus" placeholder="Write your message..." />
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                  <button class="button nw-visitor-button-send" ><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          `);
          _ref.data('id', id);
          _ref.off()
            .on('keypress', '.nw-visitor-control-chat', (e) => {
              if (e.which === 13 || e.keyCode === 13) {
                e.preventDefault();
                // call socket to send message to partner
                let _val = $(e.target).val();
                sendTo({ _id: id, message: _val });
              } else {
                // call socket to remind partner you're typing

              }
            })
            .on('click', '.nw-visitor-button-send', (e) => { // regist click on send button
              e.preventDefault();
              let _val = $(e).siblings('.nw-visitor-control-chat').val();
              sendTo({ _id: id, message: _val });
            });
          return _ref;
        };
        this.switchConversion = ({ wrapper, ref }) => {
          wrapper.find('.nw-chat-content.active').removeClass('active');
          ref.addClass('active');
        };
        this.clearInput = (id) => {
          const _input = $('.nw-chat-reply .nw-visitor-control-chat', this.exhibitionRef(id));
          _input.val('');
          _input.focus();
        };
        $('#nw-chat-toggle').off()
          .on('click', (e) => {
            e.preventDefault();
            $('.nw-exhibition-list .nw-exhibition:first-child').trigger('click');
            if (this.chatRef().length) {
              this.chatRef().addClass('active');
            }
          })
        break;

      default:
        _baseRef = $(`
          <div id="nw-visitor-chat" class="nw-visitor-chat">
            <div class="nw-visitor-chat-wrapper">
              <div class="nw-exhibition-conversion">
                <div id="profile">
                  <div class="wrap">
                    <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="" />
                    <p class="nw-user-info" style="padding-left: 10px;">
                      <?php echo $current_user->display_name . " - " . $current_user->company_name ?>
                    </p>
                  </div>
                </div>
                <div id="search">
                  <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                  <input type="text" placeholder="Search contacts..." />
                </div>
                <div class="nw-exhibition-list">
                  <ul>
                  </ul>
                </div>
                <div id="bottom-bar">
                  <button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add
                      contact</span></button>
                  <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
                </div>
              </div>
            </div>
          </div>
        `);
        this.masterRef.append(_baseRef);
        this.chatRef = () => $('#nw-visitor-chat');
        this.wrapperRef = () => $('#nw-visitor-chat > .nw-visitor-chat-wrapper');
        this.exhibitionRef = (id) => $(`.nw-chat-content`, this.wrapperRef()).filter((i, _) => $(_).data('id') == id);

        this.conversionRefs = () => $('.nw-exhibition-conversion > .nw-exhibition-list > ul');
        this.conversionRef = ({ exhibition, name, message, id }) => {
          let _title = exhibition ?? name;
          let _convRef = $(`
            <li class="nw-exhibition">
              <div class="wrap">
                <span class="contact-status online"></span>
                <img src="http://emilcarlsson.se/assets/louislitt.png" alt="" />
                <div class="meta">
                    <p class="name">${_title}</p>
                    <p class="preview">${message ?? '&nbsp;'}</p>
                </div>
                <div class="tooltip bs-tooltip-bottom" role="tooltip">
                  <div class="arrow" style="left: 52px;"></div>
                  <div class="tooltip-inner">${_title}</div>
                </div>
              </div>
            </li>
          `);
          _convRef.on('mouseenter', function () {
            $(".tooltip", $(this)).fadeToggle(400);
          });
          _convRef.on('mouseleave', function () {
            $(".tooltip", $(this)).fadeToggle(400);
          });
          _convRef.data('id', id);
          return _convRef;
        };
        this.conversionHandle = (e) => {
          let _target = $(e.target);
          _target = _target.hasClass('nw-exhibition') ? _target : _target.closest('.nw-exhibition');
          let _id = _target.data('id');
          // do something to join & fetch content of conversion with id {_id}
          this.joinTo({ _id });
        };
        this.messageRefs = (id) => $('.nw-chat-messages > .nw-room-conversion', this.exhibitionRef(id));
        this.inCommingMessage = ({ name, message, createdAt }) => $(`
          <li class="nw-message-incoming">
            <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
            <div>
              <div>
                <span class="nw-user">${name}</span>
                <span>${_toDateStr(createdAt)}</span>
              </div>
              <p>${message}</p>
            </div>
          </li>
        `);
        this.outGoingMessage = ({ message, createdAt }) => $(`
          <li class="nw-message-outgoing">
            <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
            <div>
              <div>
                <span>${_toDateStr(createdAt)}</span>
              </div>
              <p>${message}</p>
            </div>
          </li>
        `);
        this.newConversionRef = ({ id, name, sendTo }) => {
          const _ref = $(`
            <div class="nw-chat-content active">
              <div class="nw-chat-header">
                <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                <ul class="nw-heading-title">
                    <li>${name}</li>
                </ul>
              </div>
              <div class="nw-chat-messages">
                <ul class="nw-room-conversion"></ul>
              </div>
              <div class="nw-chat-reply">
                <div class="wrap">
                  <input id="input" class="nw-visitor-control-chat" type="text" autocomplete="off" autofocus="autofocus" placeholder="Write your message..." />
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                  <button class="button nw-visitor-button-send" ><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          `);
          _ref.data('id', id);
          _ref.off()
            .on('keypress', '.nw-visitor-control-chat', (e) => {
              if (e.which === 13 || e.keyCode === 13) {
                e.preventDefault();
                // call socket to send message to partner
                let _val = $(e.target).val();
                sendTo({ _id: id, message: _val });
              } else {
                // call socket to remind partner you're typing

              }
            })
            .on('click', '.nw-visitor-button-send', (e) => { // regist click on send button
              e.preventDefault();
              let _val = $(e).siblings('.nw-visitor-control-chat').val();
              sendTo({ _id: id, message: _val });
            });
          return _ref;
        };
        this.switchConversion = ({ wrapper, ref }) => {
          wrapper.find('.nw-chat-content.active').removeClass('active');
          ref.addClass('active');
        };
        this.clearInput = (id) => {
          const _input = $('.nw-chat-reply .nw-visitor-control-chat', this.exhibitionRef(id));
          _input.val('');
          _input.focus();
        };
        $('.btn-chat-with-us').off()
          .on('click', (e) => {
            e.preventDefault();
            var _modal = $(e.target).closest('.modal');
            _modal.find('.close').trigger('click');
            this.joinTo({ exhibition: post });
          })
        break;
    }
  }
  this.initialSocket = () => {
    // connect to socket server
    if (host) {
      // create & initialize connection to socket server
      var url = port ? `${host}:${port}` : host;
      transport = transport || ['websocket'];
      var socket = io(url, { transport });

      // to emit a message to socket server
      const _emit = ({ eventName, content }) => {
        if (socket) {
          socket.emit(eventName, content);
        }
      };

      // to send message to socket server
      const _sendTo = ({ _id, message }) => {
        if ((message || '').trim().length) {
          this.clearInput(_id);
          _loadMessage({ username, message, conversion: _id, createdAt: new Date() });
          _emit({ eventName: 'sendToExhibition', content: { _id, message } });
        }
      }

      // to join special exhibition
      const _joinTo = ({ _id, exhibition }) => {
        _emit({ eventName: 'logInExhibition', content: { _id, exhibition } });
      };
      this.joinTo = _joinTo;

      const _loadConversion = ({ exhibition, name, id, message }, wrapperRef) => {
        let _wrapperRef = wrapperRef || this.conversionRefs();
        let _convRef = this.conversionRef({ exhibition, name, id, message });
        _wrapperRef.append(_convRef);
        _registConversionEvent(wrapperRef, _convRef);
      }

      const _registConversionEvent = (wrapperRef, convRef) => {
        if (convRef) {
          convRef.on('click', this.conversionHandle);
        } else {
          wrapperRef.off().on('click', '.nw-exhibition', this.conversionHandle);
        }
      }

      // to add message into history list
      const _loadMessage = (content, wrapperRef) => {
        let { conversion } = content;
        let _wrapperRef = wrapperRef || this.messageRefs(conversion);
        if (_wrapperRef.length) {
          let _isIncoming = content.username !== socket.username;
          let _messageRef = _isIncoming ? this.inCommingMessage(content) : this.outGoingMessage(content);
          _wrapperRef.append(_messageRef);
          _messageRef.get(0).scrollIntoView();
        }
      }

      // to add messages into history list
      const _loadMessages = (id, messages, message) => {
        let _wrapperRef = this.messageRefs(id);
        if (_wrapperRef.length) {
          _wrapperRef.html('');
          (messages || []).forEach(content => {
            content.fromAuthor = _isVisitor(content, content.username);
            _loadMessage(content, _wrapperRef);
          });
        }
      }

      const _isVisitor = ({ conversion }, username) => socket.authors.some(_ => _.id === conversion && _.author == username);

      // authenticate with socket server
      socket.username = username;
      socket.name = name;
      _emit({ eventName: 'logInSystem', content: { username, name } });

      /**
       * define socket listener
       */

      // handle logged in socket server
      socket.on('newLoggedInSystem', (loggedInUser) => {
        const joinLog = `❣️ ${loggedInUser} has joined the chat!`;
        console.log('JOIN:', joinLog);
      });

      // handle logged in socket server
      socket.on('loggedInSystem', (loggedInUser) => {
        socket.authors = [];
        _emit({ eventName: 'fetchOldConversions', content: { mode } });
      });

      // handle when logged out from socket server
      socket.on('loggedOutSystem', (loggedOutUser) => {
        const leftLog = `🙃 ${loggedOutUser} has left the chat!`;
        console.log('OUT:', leftLog);
      });

      // handle when other user join current chat room
      socket.on('userLoggedInExhibition', ({ username, name, id }) => {
        console.log(`userLoggedInExhibition: [${name}] joined '${id}'`);
      });

      socket.on('reloadConversion', (conversion) => {
        _loadConversion(conversion);
      });

      socket.on('fetchedUserConversions', (conversion) => {
        let _wrapperRef = this.conversionRefs();
        if (_wrapperRef.length) {
          _wrapperRef.html('');
          (conversion || []).forEach(_ => _loadConversion(_, _wrapperRef));
          _registConversionEvent(_wrapperRef);
        }
      });

      // handle when logged in special exhibition
      socket.on('loggedInExhibition', ({ id, message, exhibition, activated, author, authorName }) => {
        if (!socket.authors.some(_ => _.id === id)) {
          socket.authors.push({ id, author });
        }

        if (this.chatRef && this.chatRef().length) {
          this.chatRef().addClass('active');
        }
        let _wrapper = this.wrapperRef();
        let _ref = this.exhibitionRef(id);
        if (!_ref.length) {
          let _header = _isVisitor({ conversion: id }, username) ? exhibition : authorName;
          _ref = this.newConversionRef({ id, name: _header, sendTo: _sendTo });
          _wrapper.find('.nw-chat-content.active').removeClass('active');
          _wrapper.prepend(_ref);
        } else {
          this.switchConversion({ wrapper: _wrapper, ref: _ref, id, sendTo: _sendTo });
        }
      });

      // handle with old messages from chat room
      socket.on('fetchedOldMessages', ({ id, messages, message }) => {
        _loadMessages(id, messages, message);
      });

      // handle received message from socket server into chat room
      socket.on('receiveFromExhibition', ({ username: _username, name, content }) => {
        content.name = _isVisitor(content, username) ? 'Exhibitor' : name;
        content.fromAuthor = _isVisitor(content, _username);
        _loadMessage(content);
      });
    }

  }
  this.initial();
}