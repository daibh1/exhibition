<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Virtualexpo
 */

get_header();
?>
<main id="main" class=" main-archive">
	<div class="divFull">
		<div class="category__main2">
			<div class="container">
				<?php if ( have_posts() ) : ?>
					<div class="category__main2---bg">
						<?php  the_archive_title( '<h1 class="category__main2--title">', '</h1>' ); ?>
						<div class="row">
							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post(); ?>
							<div class="col-12 col-md-4">
								<div class="category__main2--post">
									<div class="category__main2--post-images"><h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="Template 1 title"></a></h3></div>
									<h3 class="category__main2--post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo wp_trim_words( get_the_title(), 12, '...' ); ?></a></h3>
									<p><?php echo wp_trim_words( get_the_excerpt(), 32, '...' ); ?>.</p>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Read more</a>
								</div>
							</div>

							<?php endwhile; ?>
						</div>
					</div>
				<?php else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</div>
		</div><!-- end .category__main2 -->
	</div>
</main><!-- end #main -->










<?php
get_sidebar();
get_footer();
