<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package aeg_media
 */

get_header();
    
    global $post;
    $idPage = $post->ID;

?>
<div class="content container">
    <?php

    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            the_content();
        }
    }
    ?>
  <div id="wrap_admin">
    <div id="zoom" class="btnsPanel">
     <!-- <a href="javascript:void(0)" onclick="moveDrawTop()" class="upBtn"></a>
      <a href="javascript:void(0)" onclick="moveDrawRight()" class="rightBtn"></a>
      <a href="javascript:void(0)" onclick="moveDrawBottom()" class="downBtn"></a>
      <a href="javascript:void(0)" onclick="moveDrawLeft()" class="leftBtn"></a>-->
      <a href="javascript:void(0)" onclick="zoomIn()" class="zoomInBtn"></a>
      <a href="javascript:void(0)" onclick="zoomOut()" class="zoomOutBtn"></a>
    </div>
  </div>
</div>

<?php
get_footer();
?>
<script>
    let svg = d3.select("#wrap_admin").append("svg");
    let container = d3.select("#wrap_admin svg g");
    $withScreen = $(window).width();
    if ($withScreen > 1900) {
        var toaDoX = -776;
        var toaDoY = -447;
    } else if($withScreen <= 1899 && $withScreen >= 1400) {
        var toaDoX = -983;
        var toaDoY = -536;
    }else{
        var toaDoX = -1107;
        var toaDoY = -750;
    }        
    let transLat = toaDoX;
    let transLon = toaDoY;
    let transScale = 1;
    let MOVE_STEP = 20;
    let zoom = d3.zoom()
        .scaleExtent([1, 1.5])
        .translateExtent([[0, 0], [3200, 1744]])
        .on('zoom', zoomed);
    svg.call(zoom)
        .call(zoom.transform, d3.zoomIdentity.translate(transLat, transLon).scale(transScale));
    let div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0)
        .style("z-index", -1);
    jQuery(document).ready(function($) {
        console.debug('d3', d3);
        let searchParams = new URLSearchParams(window.location.search);
        $.ajax({
            type: 'GET',
            url: '<?php echo get_site_url() .'/wp-json/exhibition/v1/getBackground?idPage='. $idPage ?>',
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function (data, status, xhr) {
            if (data != '') {
                $('#zoom').addClass('show')
                let dataArray = data;
                container = svg.append("g")
                    .attr("id","background-g")
                    .attr("transform", `translate(${transLat},${transLon}) scale(${transScale})`)
                let mapImage = container.append("image")
                    .attr("xlink:href", dataArray[0].background)
                    .attr("id","background-svg")
                    .attr("width", dataArray[0].width)
            }
            $.ajax({
                type: 'GET',
                url: '<?php echo get_site_url() .'/wp-json/exhibition/v1/getJson?idPage='. $idPage ?>',
                dataType: 'json',
                processData: false,
                contentType: false
            }).done(function (data, status, xhr) {
                if (data != '') {
                    let dataArray = data;
                    let container = d3.select("#wrap_admin svg g");
                    for (let i = 0; i < dataArray.length; i++) {
                        let points = container.append("a")
                            .attr("id", dataArray[i].id)
                            .attr("xlink:href", dataArray[i].linkCompany);
                        points.on("mouseover", function(d) {
                              div.transition()
                                  .duration(200)
                                  .style("opacity", .9)
                                  .style("z-index", 1);
                              div.html("<div class='wrap-detail'> " +
                                  "<img src="+ dataArray[i].urlLogoBig +" />" +
                                  "<label class='company'>"+ dataArray[i].nameCompany + "</label>"
                                  + "<p class='description'>"+ dataArray[i].description +"</p>"
                                  + "</div>")
                                  .style("left", (event.x + 50) + "px")
                                  .style("top", (event.y - 50) + "px");
                        
                        })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(300)
                                .style("opacity", 0)
                                .style("z-index", -1);
                        });
                        let rectangle = points.append("image")
                            .attr("xlink:href", dataArray[i].urlLogo)
                            .attr("x", 67)
                            .attr("y", 4)
                            .attr("transform", dataArray[i].transform)
                            .classed('draggable', true);
                        let local = {
                            "id": dataArray[i].id,
                            "nameCompany": dataArray[i].nameCompany,
                            "urlLogo": dataArray[i].urlLogo,
                            "transform": dataArray[i].transform,
                            "description": dataArray[i].description
                        };
                    }
                }
            }).fail(function (xhr, status, error) {
                console.debug(error);
            });
        }).fail(function (xhr, status, error) {
            console.debug(error);
        });
    });
    function zoomed(event) {
        /*event.transform.x = event.sourceEvent.x;
        event.transform.y = event.sourceEvent.y;*/
        container.attr("transform", event.transform);
        console.debug('event.transform', event)
    }
    function zoomIn(){
        svg.transition().call(zoom.scaleBy, 2)
    }
    function zoomOut(){
        svg.transition().call(zoom.scaleBy, 0.5);
    }
</script>
