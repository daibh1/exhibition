<?php
 /*
 * 
 * Template Name: Post Template 3
 * Template Post Type: post
 */ 
?>
<?php get_header();the_post();
?>
<main id="main">
	<div class="divFull container">
		<div class="banner template1 template3 single-detel">
			<div class="dot-banner dot-zoom"></div>
			<img src="<?php the_post_thumbnail_url(); ?>" alt="">
            <?php if (get_field('title_information')): ?>
            	<div class="dot-banner dot-banner1">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup1">
					<div class="dot-banner--tooltip"><?php the_field('title_information') ?></div>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel"><?php the_field('title_information') ?></h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<?php the_field('company_information') ?>
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
            <?php endif; ?>

            <?php if (get_field('poster')): ?>
            	<div class="dot-banner dot-banner2 ">
				<div class="dot-banner-animation" href="#myPoster" role="button" data-toggle="modal">
					<div class="dot-banner--tooltip">Poster</div>
				</div>
				<!-- Modal -->
				<div class="modal fade popup__video" id="myPoster" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Poster</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							<div class="poster-slider slider-product--tem1 owl-theme owl-carousel">
								<?php
								$imagesPosters = get_field('poster');
								if( $imagesPosters ): ?>
							        <?php foreach( $imagesPosters as $imagesPoster ): ?>
										<div class="item"><img src="<?php echo $imagesPoster['sizes']['thumbnail']; ?>" alt="<?php echo $imagesPoster['alt']; ?>" /></div>
							        <?php endforeach; ?>
								<?php endif; ?>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php endif; ?>

            <?php if (have_rows('video')): ?>
                <div class="dot-banner dot-banner3 active">
				<div class="dot-banner-animation">
					<?php
					$v=0;
					while( have_rows('video') ): the_row();
					$t=$v++;
					$idvideo = get_sub_field('id_video_youtube');
					if ($t == 0) {
				 		echo '<iframe id="ytplayer" type="text/html" src="https://www.youtube.com/embed/'.$idvideo.'?autoplay=1&mute=1&controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
					}
					endwhile; ?>
				    <?php
				    	wp_reset_postdata();
					?>
					<div id="popup__video--slider" class="dot__popup--video"  data-toggle="modal" data-target="#popup__video"></div>
					<!-- Modal -->
					<div class="modal fade popup__video" id="popup__video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					    	<div class="modal-header">
					        	<h5 class="modal-title" id="exampleModalLabel">Video</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					    	</div>
					    	<div class="modal-body" id="wrap-video">
					    	</div>
					    </div>
					  </div>
					</div>
					<!-- ENd Modal -->
				</div>
			</div>
            <?php endif; ?>

            <?php if (have_rows('document')): ?>
            	<div class="dot-banner dot-banner4">
				<div class="dot-banner-animation" href="#myModal0" role="button" data-toggle="modal">
					<div class="dot-banner--tooltip">Document Info</div>
				</div>
				<div id="myModal0" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel"><?php the_field('title_document') ?></h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
				            <div class="modal-body">
				            	<div class="document-table">
				            		<?php
			            			$document1 = 1;
				            		while( have_rows('document') ): the_row();
			            			$document2 = $document1++;
			            			?>
						        	<div class="row">
				            			<div class="col-1"><img src="<?php echo get_template_directory_uri(); ?>/images/google-docs.svg" alt="docs"></div>
				            			<div class="col-2"><a href="#pop-doccument<?php echo $document2; ?>" role="button" class="btn" data-toggle="modal">View</a></div>
                                        <div class="col-7" id="document-name"><?php the_sub_field('title'); ?></div>
                                        <div class="col-1">
                                            <a href="javascript:void(0)" id="add-to-cart"><img src="<?php echo get_template_directory_uri(); ?>/images/shopping-cart.svg" alt=""></a>
                                        </div>
				            			<div class="col-1">
				            				<a target="_blank" id="document-link" data-id="<?php the_sub_field('id_document');?>" href="https://drive.google.com/file/d/<?php the_sub_field('id_document'); ?>/view"><img src="<?php echo get_template_directory_uri(); ?>/images/download.svg" alt=""></a>
				            			</div>
				            		</div>
								    <?php endwhile; ?>
				            	</div>
                                <div class="alert alert-success alert-dismissible fade" role="alert" id="buttonAlert">
<!--                                    <strong>Document is added successfully!</strong>-->
                                    <strong></strong>
                                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<?php
        		if( have_rows('document') ):
    			$document3 = 1;
        		while( have_rows('document') ): the_row();
    			$document4 = $document3++;
    			?>
				<div id="pop-doccument<?php echo $document4; ?>" class="modal modal-child modal-pdf" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
				    <div class="modal-dialog">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel"><?php the_sub_field('title'); ?></h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
				            <div class="modal-body">
				                <iframe src="https://drive.google.com/file/d/<?php the_sub_field('id_document'); ?>/preview" frameborder="0"></iframe>
				            </div>
				        </div>
				    </div>
				</div>
			    <?php endwhile; ?>
				<?php endif; ?>
			</div>
            <?php endif; ?>

            <?php if (get_field('product_show')): ?>
            	<div class="dot-banner dot-banner5">
				<div class="dot-banner-animation">
					<div class="slider-product--tem3 owl-theme owl-carousel">
						<?php
						$featured_posts = get_field('product_show');
						$x1=0;
						$x2=0;
						if( $featured_posts ): ?>
							<?php foreach( $featured_posts as $post ):
								$y1=$x1++;
								setup_postdata($post); ?>

								<div class="item">
									<div class="dot-banner-animation" data-toggle="modal" data-target="#sp<?php echo $y1; ?>">
										<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
									</div>
								</div>
							<?php endforeach; ?>
							<?php
							wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
				</div>

			</div>
            <?php endif; ?>

			<?php
			if( $featured_posts ): ?>
				<?php foreach( $featured_posts as $post ): 
					$y2=$x2++;
					setup_postdata($post); ?>
					<!-- Modal -->
					<div class="modal fade modalsp" id="sp<?php echo $y2; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel"><?php the_title(); ?></h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      	<div id="post" class="content-primer">
					      		<div class="row">
					      			<div class="col-12">
					      				<?php the_content(); ?>
										<p>&nbsp;</p>
					      			</div>
					      			<div class="col-12 col-md-6">
					      				<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
					      			</div>
					      			<div class="col-12 col-md-6">
					      				<?php if( have_rows('basic_information') ): ?>
											<div class="item">
												<h4>Basic parameters</h4>
												<table align="left" class="contenttable">
													<tbody>
														<?php while( have_rows('basic_information') ): the_row(); ?>
															<tr>
																<td>
																	<p><strong><?php the_sub_field('name_information'); ?>:</strong>
																	</p>
																</td>
																<td>
																	<p><?php the_sub_field('information'); ?></p>
																</td>
															</tr>
														<?php endwhile; ?>
													</tbody>
												</table>
											</div>
										<?php endif; ?>
					      			</div>
					      		</div>
					      	</div>
					      </div>
					    </div>
					  </div>
					</div>
					<!-- ENd Modal -->
				<?php endforeach; ?>
				<?php 
				wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php if (get_field('banner')): ?>
				<div class="dot-banner dot-banner6">
				<div class="dot-banner-animation" href="#myBanner" role="button" data-toggle="modal">
					<div class="dot-banner--tooltip">Banner</div>
				</div>
				<!-- Modal -->
				<div class="modal fade popup__video" id="myBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" >
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Banner</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							<div class="poster-slider slider-product--tem1 owl-theme owl-carousel">
								<?php
								$imagesBanners = get_field('banner');
								if( $imagesBanners ): ?>
							        <?php foreach( $imagesBanners as $imagesBanner ): ?>
										<div class="item"><img src="<?php echo $imagesBanner['sizes']['thumbnail']; ?>" alt="<?php echo $imagesBanner['alt']; ?>" /></div>
							        <?php endforeach; ?>
								<?php endif; ?>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php endif; ?>

		</div>
	</div>

    <!-- start  popup register video call  -->
    <div id="modal-schedule-video-call"  class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Schedule video calls</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" id="content">
                    <label for="messenger">Messenger </label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="messenger" value="I want to make a call with you right now!" required>
                    </div>

                    <div class="alert alert-success alert-dismissible fade " role="alert" id="buttonAlertSchedule">
                        <strong>Schedule Success!</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="send-request"> Submit </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end  popup register video call  -->
</main><!-- end #main -->

<div class="end"></div>

<ul class="nav-v-single">
	<li>
		<a title="Chat with Me" href="javascript:void(Tawk_API.toggle())"><i class="fa fa-comments-o" aria-hidden="true"></i></a>
	</li>
    <li>
        <a  title="Schedule" data-toggle="modal" data-target="#order-booth"><i class="fa fa-calendar" aria-hidden="true"></i></a>
    </li>
	<li>
        <a href="#" id="show-modal-schedule-video-call" title="To use the call meeting function, please make a suggestion to our staffs. "><i class="fa fa-video-camera" aria-hidden="true"></i></a>
	</li>
</ul>

<?php get_footer(); ?>


<script type="text/javascript">
jQuery(document).ready(function() {
	var i = 0;
	$( "#popup__video--slider" ).on( "click", function() {
		var y = i++;
		if ( y == 0 ) {
		let item = '<div id="slider-video" class="popup__video--slider owl-theme owl-carousel"><?php 
			if( have_rows('video') ):
			while( have_rows('video') ): the_row(); 
			$idvideo = get_sub_field('id_video_youtube');
			$first_video = '<div class="item"><iframe src="https://www.youtube.com/embed/';
			$end_video = '?enablejsapi=1&html5=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
					echo $first_video.$idvideo.$end_video;
			endwhile; ?>
			<?php
			wp_reset_postdata(); 
			endif; 
			?></div>';
	  	$('#wrap-video').append(item);
	  	jQuery(".popup__video--slider.owl-carousel").owlCarousel({
			loop:false,
			margin:20,
		    items:1,
			autoplay: false,
			nav:true,
			dots:false,
			navText: ["‹","›"],
		});
		};
		<?php 	
			if( have_rows('video') ): 
			$i= 0;
		?>
		jQuery(".popup__video--slider .owl-nav div").click(function(){
		<?php 
			while( have_rows('video') ): the_row();
			$y = $i++;
		?>
			jQuery('.popup__video--slider .item iframe')[<?php echo $y; ?>].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
			<?php endwhile; ?>
			});
			<?php
			wp_reset_postdata(); 
			endif; 
		?>
		<?php 	
			if( have_rows('video') ): 
			$i= 0;
		?>
		jQuery(".popup__video .close").click(function(){
		<?php 
			while( have_rows('video') ): the_row();
			$y = $i++;
		?>
			jQuery('.popup__video--slider .item iframe')[<?php echo $y; ?>].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
			<?php endwhile; ?>
			});
			<?php
			wp_reset_postdata(); 
			endif; 
		?>
	});

    // show popup Schedule video calls
    $('#show-modal-schedule-video-call').on('click', function () {
        $('#modal-schedule-video-call').modal('show');
    })

});
</script>
<!--
<?php the_field('code_add_more') ?>
-->
<?php global $current_user; wp_get_current_user(); ?>
<?php if ( is_user_logged_in() ) { 
?>


<script type="text/javascript">
var Tawk_API=Tawk_API||{};
var Tawk_LoadStart=new Date();


Tawk_API.onLoad = function(){
Tawk_API.setAttributes({
'name' : '<?php echo $current_user->user_login . " - " . $current_user->company_name ?>',
'email': '<?php echo $current_user->user_email ?>',
'hash' : 'hash-value'
}, function (error) {});

};
(function(){
    jQuery('#send-request').on('click', function () {
        var messenger = $('#content').find('input[name="messenger"]').val();

        Tawk_API.addEvent('Schedule-video-calls', {
            'messenger'  : messenger
        }, function(error){
            if (error)
            {
                return alert(error)
            }
            $("#buttonAlertSchedule").fadeTo(2000, 500).slideUp(500, function() {
                $("#buttonAlertSchedule").slideUp(500);
            });
        });
    })
})();
</script>

<?php } else { wp_loginout(); } ?>
<!-- Modal Exhibition Name -->
<div class="modal fade" id="order-booth" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[contact-form-7 id="622" title="Form booth"]') ?>
      </div>
    </div>
  </div>
</div>
<!-- ENd Modal -->
