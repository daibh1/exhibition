<?php
/**
 * Virtualexpo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Virtualexpo
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'virtualexpo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function virtualexpo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Virtualexpo, use a find and replace
		 * to change 'virtualexpo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'virtualexpo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'virtualexpo_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'virtualexpo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function virtualexpo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'virtualexpo_content_width', 640 );
}
add_action( 'after_setup_theme', 'virtualexpo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function virtualexpo_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'virtualexpo' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'virtualexpo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'virtualexpo_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* Add Menu */
register_nav_menus(array('primary' => 'Menu Primary'));
register_nav_menus(array('login' => 'Login Logout'));
register_nav_menus(array('menuft' => 'Menu Footer'));



include_once dirname(__FILE__) . '/widget/home-widget.php';


// Thay doi duong dan logo admin



function wpc_url_login(){
return "#"; // duong dan vao website cua ban
}
add_filter('login_headerurl', 'wpc_url_login');
add_theme_support( 'post-thumbnails', array('page','post','product') );





// Remove Parent Category from Child Category URL

add_filter('term_link', 'aeg_media_no_category_parents', 1000, 3);
function aeg_media_no_category_parents($url, $term, $taxonomy) {

    if($taxonomy == 'category'){

        $term_nicename = $term->slug;

        $url = trailingslashit(get_option( 'home' )) . user_trailingslashit( $term_nicename, 'category' );

    }
    return $url;
}

// Rewrite url mới

function aeg_media_no_category_parents_rewrite_rules($flash = false) {
    $terms = get_terms( array(
        'taxonomy' => 'category',
        'post_type' => 'post',
        'hide_empty' => false,
    ));
    if($terms && !is_wp_error($terms)){
        foreach ($terms as $term){
            $term_slug = $term->slug;
            add_rewrite_rule($term_slug.'/?$', 'index.php?category_name='.$term_slug,'top');
            add_rewrite_rule($term_slug.'/page/([0-9]{1,})/?$', 'index.php?category_name='.$term_slug.'&paged=$matches[1]','top');
            add_rewrite_rule($term_slug.'/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?category_name='.$term_slug.'&feed=$matches[1]','top');
        }
    }
    if ($flash == true)
        flush_rewrite_rules(false);
}

add_action('init', 'aeg_media_no_category_parents_rewrite_rules');



/*Sửa lỗi khi tạo mới category bị 404*/
function aeg_media_new_category_edit_success() {
    aeg_media_no_category_parents_rewrite_rules(true);
}
add_action('created_category','aeg_media_new_category_edit_success');
add_action('edited_category','aeg_media_new_category_edit_success');
add_action('delete_category','aeg_media_new_category_edit_success');



// Xóa chữ chuyên mục ở Category

function themewp_archive_title( $title ) {

    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {

        $title = post_type_archive_title( '', false );

    } elseif ( is_tax() ) {

        $title = single_term_title( '', false );

    }

  

    return $title;

}



add_filter( 'get_the_archive_title', 'themewp_archive_title' );



// Allow svg

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');
show_admin_bar( false );





if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Setting Website');
}

function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    $mime_types['psd'] = 'image/vnd.adobe.photoshop'; //Adding photoshop files
    return $mime_types;

}

add_filter('upload_mimes', 'my_myme_types', 1, 1);


/* Add editing function */
function ilc_mce_buttons($buttons){

  array_push($buttons,
     "backcolor",
     "anchor",
     "hr",
     "sub",
     "sup",
     "fontselect",
     "fontsizeselect",
     "styleselect",
     "cleanup"
);
  return $buttons;
}
add_filter("mce_buttons", "ilc_mce_buttons");

//Remove Default WordPress Image Sizes

function svl_remove_default_image_sizes( $sizes) {
unset( $sizes['thumbnail']);
unset( $sizes['medium']);
unset( $sizes['large']);



return $sizes;

}

add_filter('intermediate_image_sizes_advanced', 'svl_remove_default_image_sizes');

add_filter('intermediate_image_sizes_advanced','__return_false');



// Remove last ver url

function themewp_remove_script_version( $src ){

$parts = explode( '?ver', $src );

return $parts[0];

}

add_filter( 'script_loader_src', 'themewp_remove_script_version', 15, 1 );

add_filter( 'style_loader_src', 'themewp_remove_script_version', 15, 1 );





//Remove wpadminbar

add_filter( 'show_admin_bar', '__return_false' );



// Remove XML-RPC

function remove_x_pingback($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}
add_filter('wp_headers', 'remove_x_pingback');
add_filter('xmlrpc_enabled', '__return_false');



// Remove upload plugins và themes

// function __block_caps( $caps, $cap )
// {
//     if ( $cap === 'upload_plugins' || $cap === 'install_themes')
//         $caps[] = 'do_not_allow';
//     return $caps;
// }
// add_filter( 'map_meta_cap', '__block_caps', 10, 2 );



//SMTP send form

add_action( 'phpmailer_init', 'configure_smtp' );
function configure_smtp( PHPMailer $phpmailer ){
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.gmail.com';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 465;
    $phpmailer->Username = 'mr.vuong1995@gmail.com'; // email
    $phpmailer->Password = 'qxawhksmofakohwd'; // pass 2
    $phpmailer->SMTPSecure = true;
    $phpmailer->From = 'admin-website@thienhamedia.com';
    $phpmailer->FromName='Admin Website';

}
// Remove new editing
add_filter('use_block_editor_for_post', '__return_false');



function aeg_mediastyle() {
    wp_enqueue_script('script_1', get_template_directory_uri() . '/js/jquery.js',1,'',true);
    wp_enqueue_script('script_2', get_template_directory_uri() . '/js/bootstrap.min.js',1,'',true);
    wp_enqueue_script('script_7', get_template_directory_uri() . '/js/bootstrap-datetimepicker.min.js',1,'',true);
    wp_enqueue_script('d3js', get_template_directory_uri() . '/js/d3.js',1,'',false);
    wp_enqueue_script('script_3', get_template_directory_uri() . '/js/owl.carousel.min.js',1,'',true);
    wp_enqueue_script('datatable_js', get_template_directory_uri() . '/js/datatables.min.js',1,'',true);
    wp_enqueue_script('script_4', get_template_directory_uri() . '/js/script.js', array( 'jquery'),1,'',false);
    wp_localize_script('script_4', 'ObjectCommon', array(
            'siteUrl' => __(get_site_url())
        )
    );

    if (is_user_logged_in())
    {
        $current_user =  wp_get_current_user();
        wp_localize_script('script_4', 'ObjectCommon', array(
                'userId' => __($current_user->ID),
                'siteUrl' => __(get_site_url())
            )
        );
    }

    wp_enqueue_style('css_0', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('css_4', get_template_directory_uri() . '/css/bootstrap-datetimepicker.min.css');
    wp_enqueue_style('datatable_css', get_template_directory_uri() . '/css/datatables.min.css');
    wp_enqueue_style('css_1', get_template_directory_uri() . '/css/owl.carousel.min.css');
    wp_enqueue_style('css_2', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('css_3', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('css_3_1', get_template_directory_uri() . '/css/exhibition-chat.css');

}

add_action('wp_enqueue_scripts', 'aeg_mediastyle');







function product_func() {
    $labels = array(
        'name' => _x('Product', 'Post Type General Name', 'aeg_media'),
        'singular_name' => _x('Product', 'Post Type Singular Name', 'aeg_media'),
        'menu_name' => __('Product', 'aeg_media'),
        'name_admin_bar' => __('Product', 'aeg_media'),
        'archives' => __('Item Archives', 'aeg_media'),
        'attributes' => __('Item Attributes', 'aeg_media'),
        'parent_item_colon' => __('Parent Item:', 'aeg_media'),
        'all_items' => __('All products', 'aeg_media'),
        'add_new_item' => __('Product title', 'aeg_media'),
        'add_new' => __('Add new product', 'aeg_media'),
        'new_item' => __('New Item', 'aeg_media'),
        'edit_item' => __('Edit Item', 'aeg_media'),
        'update_item' => __('Update Item', 'aeg_media'),
        'view_item' => __('View Item', 'aeg_media'),
        'view_items' => __('View Items', 'aeg_media'),
        'search_items' => __('Search Item', 'aeg_media'),
        'not_found' => __('Not found', 'aeg_media'),
        'not_found_in_trash' => __('Not found in Trash', 'aeg_media'),
        'featured_image' => __('Featured image', 'aeg_media'),
        'set_featured_image' => __('Set featured image', 'aeg_media'),
        'remove_featured_image' => __('Remove featured image', 'aeg_media'),
        'use_featured_image' => __('Use as featured image', 'aeg_media'),
        'insert_into_item' => __('Insert into item', 'aeg_media'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'aeg_media'),
        'items_list' => __('Items list', 'aeg_media'),
        'items_list_navigation' => __('Items list navigation', 'aeg_media'),
        'filter_items_list' => __('Filter items list', 'aeg_media'),
    );
    $rewrite = array(
        'slug' => _x('product', 'slug', 'aeg_media'), //Slug  product
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'label' => __('product', 'aeg_media'),
        'description' => __('Bài viết product', 'aeg_media'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments', ),
        'taxonomies' => array('product_cat'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-cart',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => 'product', // archive slug
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => $rewrite,
        'capability_type' => 'page',
    );
    register_post_type('product', $args);
}
add_action('init', 'product_func', 0);

// Register Custom Taxonomy
function product_cat_product_func() {
    $labels = array(
        'name' => _x('Product Category', 'Taxonomy General Name', 'aeg_media'),
        'singular_name' => _x('Product Category', 'Taxonomy Singular Name', 'aeg_media'),
        'menu_name' => __('Product Category', 'aeg_media'),
        'all_items' => __('All Product Category', 'aeg_media'),
        'parent_item' => __('Parent Category', 'aeg_media'),
        'parent_item_colon' => __('Parent Item:', 'aeg_media'),
        'new_item_name' => __('New Item Name', 'aeg_media'),
        'add_new_item' => __('Add New Category', 'aeg_media'),
        'edit_item' => __('Edit Item', 'aeg_media'),
        'update_item' => __('Update Item', 'aeg_media'),
        'view_item' => __('View Item', 'aeg_media'),
        'separate_items_with_commas' => __('Separate items with commas', 'aeg_media'),
        'add_or_remove_items' => __('Add or remove items', 'aeg_media'),
        'choose_from_most_used' => __('Choose from the most used', 'aeg_media'),
        'popular_items' => __('Popular Items', 'aeg_media'),
        'search_items' => __('Search Items', 'aeg_media'),
        'not_found' => __('There are no categories', 'aeg_media'),
        'no_terms' => __('No items', 'aeg_media'),
        'items_list' => __('Items list', 'aeg_media'),
        'items_list_navigation' => __('Items list navigation', 'aeg_media'),
    );
    $rewrite = array(
        'slug' => _x('product', 'slug', 'aeg_media'),
        'with_front' => true,
        'hierarchical' => true,
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => $rewrite,
    );
    register_taxonomy('product_cat', array('product'), $args);
}
add_action('init', 'product_cat_product_func', 0);


/**
 * Display a custom taxonomy dropdown in admin
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'product_cat'; // change to your taxonomy
    if ($typenow == $post_type) {
        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => sprintf( __( 'All %s', 'textdomain' ), $info_taxonomy->label ),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ));
    };
}


add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'product_cat'; // change to your taxonomy
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}


add_filter( 'wp_unique_post_slug', 'prefix_wp_unique_post_slug', 2, 6 );
function prefix_wp_unique_post_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ) {

$slug = sanitize_title($slug);


return $slug;
}


/**
 * Customizer theme_setting.
 */
require get_template_directory() . '/inc/setting-theme.php';

add_filter( 'big_image_size_threshold', '__return_false' );







function filter_next_post_sort($sort) {
    global $post;
    if (get_post_type($post) == 'post') {
        $sort = "ORDER BY p.post_title ASC LIMIT 1";
    }
    else{
        $sort = "ORDER BY p.post_date ASC LIMIT 1";
    }
    return $sort;
}
function filter_next_post_where($where) {
    global $post, $wpdb;
    if (get_post_type($post) == 'post') {
        return $wpdb->prepare("WHERE p.post_title > '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'",$post->post_title);
    }
    else{
        return $wpdb->prepare( "WHERE p.post_date > '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'", $post->post_date);
    }
}

function filter_previous_post_sort($sort) {
    global $post;
    if (get_post_type($post) == 'post') {
        $sort = "ORDER BY p.post_title DESC LIMIT 1";
    }
    else{
        $sort = "ORDER BY p.post_date DESC LIMIT 1";
    }
    return $sort;
}
function filter_previous_post_where($where) {
    global $post, $wpdb;
    if (get_post_type($post) == 'post') {
        return $wpdb->prepare("WHERE p.post_title < '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'",$post->post_title);
    }
    else{
        return $wpdb->prepare( "WHERE p.post_date < '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'", $post->post_date);
    }
}

add_filter('get_next_post_sort',   'filter_next_post_sort');
add_filter('get_next_post_where',  'filter_next_post_where');

add_filter('get_previous_post_sort',  'filter_previous_post_sort');
add_filter('get_previous_post_where', 'filter_previous_post_where');