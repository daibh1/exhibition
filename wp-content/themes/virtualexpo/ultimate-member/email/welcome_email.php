<p>Hi {display_name},</p>
<p>Thank you for signing up with {site_name}! Your account has been approved and is now active.</p>
<p>To login please visit the following url:</p>
<p>{login_url}</p>
<p>Your account e-mail: {email}<br />Your account username: {username}<br />Set your account password: {password_reset_link}</p>
<p>If you have any problems, please contact us at {admin_email}</p>
<p>Thanks,<br />{site_name}</p>