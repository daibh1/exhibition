<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Virtualexpo
 */

get_header();
?>
<?php
$currentlang = get_bloginfo('language');
if($currentlang=="en-US"):
	$linktrang = get_site_url().'/booth/';
 elseif($currentlang=="vi"):
	$linktrang = get_site_url().'/gian-hang/';
 endif; ?>
<main id="main">
	<div class="divFull container single-detel">
		<div class="banner template1">
			<img src="<?php the_post_thumbnail_url(); ?>" alt="">
			<div class="dot-banner dot-banner1">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup1">
					<div class="dot-banner--tooltip"><?php the_field('title_information') ?></div>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel"><?php the_field('title_information') ?></h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<?php the_field('company_information') ?>
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
			<div class="dot-banner dot-banner3 active">
				<div class="dot-banner-animation">
					<?php
					if( have_rows('video') ):
					$v=0;
					while( have_rows('video') ): the_row();
					$t=$v++;
					$idvideo = get_sub_field('id_video_youtube');
					if ($t == 0) {
				 		echo '<iframe id="ytplayer" type="text/html" src="https://www.youtube.com/embed/'.$idvideo.'?autoplay=1&cc_load_policy=1" frameborder="0" allowfullscreen></iframe>';
					}
					endwhile; ?>
				    <?php
				    	wp_reset_postdata();
						endif;
					?>
					<div id="popup__video--slider" class="dot__popup--video"  data-toggle="modal" data-target="#popup__video"></div>
					<!-- Modal -->
					<div class="modal fade popup__video" id="popup__video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					    	<div class="modal-header">
					        	<h5 class="modal-title" id="exampleModalLabel">Video</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					    	</div>
					    	<div class="modal-body" id="wrap-video">
					    	</div>
					    </div>
					  </div>
					</div>
					<!-- ENd Modal -->
				</div>
			</div>
			<div class="dot-banner dot-banner4 ">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup4">
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Poster</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<img class="img-poster-banner" src="<?php the_field('poster_1') ?>" alt="poster 1">
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
			<div class="dot-banner dot-banner5">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup5">
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Poster</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<img class="img-poster-banner" src="<?php the_field('poster_2') ?>" alt="poster 2">
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
			<div class="dot-banner dot-banner6">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup6">
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Banner</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<img class="img-poster-banner" src="<?php the_field('banner_1') ?>" alt="Banner 1">
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
			<div class="dot-banner dot-banner7">
				<div class="dot-banner-animation" data-toggle="modal" data-target="#popup7">
				</div>
				<!-- Modal -->
				<div class="modal fade" id="popup7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Banner</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<img class="img-poster-banner" src="<?php the_field('banner_2') ?>" alt="Banner 2">
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			</div>
			<div class="dot-banner dot-banner8">
				<div class="dot-banner-animation" href="#myModal0" role="button" data-toggle="modal">
					<div class="dot-banner--tooltip"><?php pll_e('Document Info'); ?></div>
				</div>
				<div id="myModal0" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel"><?php the_field('title_document') ?></h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
				            <div class="modal-body">
				            	<div class="document-table">
				            		<?php
				            		if( have_rows('document') ):
			            			$document1 = 1;
				            		while( have_rows('document') ): the_row();
			            			$document2 = $document1++;
			            			?>
						        	<div class="row">
				            			<div class="col-1"><img src="<?php echo get_template_directory_uri(); ?>/images/google-docs.svg" alt="docs"></div>
				            			<div class="col-2"><a href="#pop-doccument<?php echo $document2; ?>" role="button" class="btn" data-toggle="modal"><?php pll_e('View'); ?></a></div>
				            			<div class="col-8"><?php the_sub_field('title'); ?></div>
				            			<div class="col-1">
				            				<a target="_blank" href="https://drive.google.com/file/d/<?php the_sub_field('id_document'); ?>/view"><img src="<?php echo get_template_directory_uri(); ?>/images/download.svg" alt=""></a>
				            			</div>
				            		</div>
								    <?php endwhile; ?>
									<?php endif; ?>
				            	</div>
				            </div>
				        </div>
				    </div>
				</div>
				<?php
        		if( have_rows('document') ):
    			$document3 = 1;
        		while( have_rows('document') ): the_row();
    			$document4 = $document3++;
    			?>
				<div id="pop-doccument<?php echo $document4; ?>" class="modal modal-child modal-pdf" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
				    <div class="modal-dialog">
				        <!-- Modal content-->
				        <div class="modal-content">
				            <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel"><?php the_sub_field('title'); ?></h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
				            <div class="modal-body">
				                <iframe src="https://drive.google.com/file/d/<?php the_sub_field('id_document'); ?>/preview" frameborder="0"></iframe>
				            </div>
				        </div>
				    </div>
				</div>
			    <?php endwhile; ?>
				<?php endif; ?>













			</div><!-- end  .dot-banner8-->









			<div class="dot-banner dot-banner9 sliderproduct-tem1">
				<div class="dot-banner-animation">
					<div class="slider-product--tem1 owl-theme owl-carousel">
						<?php
						$featured_posts = get_field('product_show');
						$x1=0;
						$x2=0;
						if( $featured_posts ): ?>
							<?php foreach( $featured_posts as $post ):
								$y1=$x1++;
								setup_postdata($post); ?>
								
								<div class="item">
									<div class="dot-banner-animation" data-toggle="modal" data-target="#sp<?php echo $y1; ?>">
										<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
									</div>
								</div>
							<?php endforeach; ?>
							<?php
							wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php
		if( $featured_posts ): ?>
			<?php foreach( $featured_posts as $post ):
				$y2=$x2++;
				setup_postdata($post); ?>
				<!-- Modal -->
				<div class="modal fade modalsp" id="sp<?php echo $y2; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel"><?php the_title(); ?></h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div id="post" class="content-primer">
				      		<div class="row">
				      			<div class="col-12">
				      				<?php the_content(); ?>
									<p>&nbsp;</p>
				      			</div>
				      			<div class="col-12 col-md-6">
				      				<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
				      			</div>
				      			<div class="col-12 col-md-6">
				      				<?php if( have_rows('basic_information') ): ?>
										<div class="item">
											<h4><?php pll_e('Basic parameters'); ?></h4>
											<table align="left" class="contenttable">
												<tbody>
													<?php while( have_rows('basic_information') ): the_row(); ?>
														<tr>
															<td>
																<p><strong><?php the_sub_field('name_information'); ?>:</strong>
																</p>
															</td>
															<td>
																<p><?php the_sub_field('information'); ?></p>
															</td>
														</tr>
													<?php endwhile; ?>
												</tbody>
											</table>
										</div>
									<?php endif; ?>
				      			</div>
				      		</div>
				      	</div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- ENd Modal -->
			<?php endforeach; ?>
			<?php
			wp_reset_postdata(); ?>
		<?php endif; ?>
		</div>
	</div>

</main><!-- end #main -->
<script src="https://apis.google.com/js/platform.js" async defer></script>



<?php
get_footer();

?>

<script type="text/javascript">
window.addEventListener("load", function(event) {
	$( "#popup__video--slider" ).on( "click", function() {
		let item = '<div id="slider-video" class="popup__video--slider owl-theme owl-carousel"><?php
if( have_rows('video') ):
while( have_rows('video') ): the_row();
$idvideo = get_sub_field('id_video_youtube');
$first_video = '<div class="item"><iframe src="https://www.youtube.com/embed/';
$end_video = '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
		echo $first_video.$idvideo.$end_video;
endwhile; ?>
<?php
wp_reset_postdata();
endif;
?></div>'
	  	 $('#wrap-video').append(item);
	  	 jQuery(".popup__video--slider.owl-carousel").owlCarousel({
			loop:true,
			margin:20,
		    items:1,
			autoplay:false,
			nav:true,
			dots:false,
			navText: ["‹","›"],
		});
	});
});

</script>


