<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Virtualexpo
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php the_field('code_head','option') ?>
</head>

<body <?php body_class(); ?>>
<div id="body">
<?php the_field('code_body','option') ?>	
<?php wp_body_open(); ?>
	<header id="header">
		<div class="header__topbar2">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-3 header__topbar2--left">
						<?php 
							if (is_page(11) or is_home()  or is_page(13)) :
							?>
							<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php the_field('logo','option') ?>" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
							<?php
						else :
							?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php the_field('logo','option') ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
							<?php
						endif; ?>
					</div>
					<div class="col-12 col-md-9 header__topbar2--right">
						<?php if( have_rows('logo_partner', 'option') ): ?>
						<span>Sponsors</span>
						<div class="header__topbar2--marquee">
							<ul class="scroller1">
							    <?php while( have_rows('logo_partner', 'option') ): the_row(); ?>
							        <li>
							        	<a href="<?php the_sub_field('link'); ?>" title="logo-brand"><img src="<?php the_sub_field('logo'); ?>" alt="logo-brand"></a>
							        </li>
							    <?php endwhile; ?>
							</ul>
							<ul  class="scroller2">
							    <?php while( have_rows('logo_partner', 'option') ): the_row(); ?>
							        <li>
							        	<a href="<?php the_sub_field('link'); ?>" title="logo-brand"><img src="<?php the_sub_field('logo'); ?>" alt="logo-brand"></a>
							        </li>
							    <?php endwhile; ?>
							</ul>
						</div>
						<?php endif; ?>
						<div class="profile-header">
							<a>
								<span>
									<i class="fa fa-user-o" aria-hidden="true"></i>
								</span>
								<?php 
								if ( is_user_logged_in() ) : ?>
									<i class="fa fa-check-circle" aria-hidden="true"></i>
								<?php else : ?>
									<i class="fa fa-minus-circle" aria-hidden="true"></i>
								<?php endif ?>
							</a>
							<ul id="menu-menu-login" class="">
								<?php 
								if ( is_user_logged_in() ) : ?>
									<?php 
									$user = wp_get_current_user();
									$roleUser = $user->roles[0];
									?>
									<?php if($roleUser == "subscriber" or $roleUser == "um_visitor"): ?>
									<?php else : ?>
									<li><a href="<?php echo get_site_url(); ?>/chat-room">Booth management</a></li> 
									<li><a id="nw-chat-toggle">Open chat</a></li> 
									<?php endif; ?>
									<li><a href="<?php echo get_site_url(); ?>/user/">Profile Setting</a></li> 
									<li><a href="<?php echo get_site_url(); ?>/logout/">Logout</a></li>
								<?php else : ?>
									<li><a href="<?php echo get_site_url(); ?>/login/">Login</a></li>
									<li><a data-toggle="modal" data-target="#popup-egistration" style="cursor: pointer;">Register</a></li>
								<?php endif ?>
							</ul>	
						</div>
					</div>
				</div>
			</div>
		</div><!-- end .header__topbar2 -->
	</header><!-- end #header -->

