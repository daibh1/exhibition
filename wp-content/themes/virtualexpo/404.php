<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Virtualexpo
 */

get_header();
?>

<div class="content-page">

<section class="main-contact">
<div class="container rigistra-body">
<div class="page_content_">
<p style="font-size:20px;text-align:center"><span style="font-size:30px;font-weight:bold">Error 404</span></br></br>Page does not exist. Return <a style="color: blue;"  href="<?php echo site_url()?>"> Home page</a></p>

</div>
</div>
</section>

</div>
<?php
get_footer();
