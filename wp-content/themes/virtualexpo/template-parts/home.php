<?php /** * Template Name: Home page */ ?>
<?php get_header();the_post();?>
<div class="previewLoadHome">
	<div class="div-zoom"> 
		<video autoplay muted id="video--wellcome" >
			<source src="<?php echo get_template_directory_uri(); ?>/video/wellcome.mp4" type="video/mp4">
		</video>
		<div class="close--div-zoom"><a style="width: 100%;height: 100%;" href="<?php bloginfo('url') ?>/lobby"><img src="<?php echo get_template_directory_uri(); ?>/images/enter-hall.png" alt=""></a></div>
	</div>
</div> 
<?php get_footer(); ?>
<script type="text/javascript">
    var aud = document.getElementById("video--wellcome");
    aud.onended = function() {
        jQuery(".close--div-zoom").addClass("active");
    };
    var ua = navigator.userAgent.toLowerCase();
	var is_safari = (ua.indexOf("safari/") > -1 && ua.indexOf("chrome") < 0);
	if(is_safari) {
	    var video = document.getElementById('#video--wellcome');
	    setTimeout(function() {
	       video.play();
	    }, 50);
	}  
</script>