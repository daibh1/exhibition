<?php /** * Template Name: lobby */ ?>
<?php get_header();the_post();?>
<main id="main">
	<div class="container">
		<div class="element__all">
			<div class="element--1">
				<div id="countdown"></div>
				<img src="<?php the_post_thumbnail_url(); ?>" alt="">
				<div class=" element--1--home element--1--banner1">
					<?php the_field('iframe_video_home'); ?>
				</div>

				<div class=" element--1--home element--1--banner2">
					<a href="#"><img src="<?php the_field('banner_1') ?>" alt=""></a>
				</div>
				<div class=" element--1--home element--1--banner3">
					<a href="#"><img src="<?php the_field('banner_2') ?>" alt=""></a>
				</div>
				<div class=" element--1--home element--1--banner4">
					<div class="dot-banner-animation" data-toggle="modal" data-target="#popup1">
						<div class="dot-banner--tooltip">Information</div>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      	<div id="post" class="content-primer">
					      		<?php the_field('Information') ?>
					      	</div>
					      </div>
						  <?php
							$isVisitor = !is_user_logged_in();

							if (!$isVisitor) {
								global $current_user;
								$roleUser = $current_user->roles[0];
								$isVisitor = ($roleUser == "subscriber" or $roleUser == "um_visitor");
							}

							if ($isVisitor) :
						  ?>
							<div class="modal-footer">
								<div><button class="btn btn-outline-success btn-chat-with-us">Chat with US</button></div>
							</div>
						  <?php endif; ?>
					    </div>
					  </div>
					</div>
					<!-- ENd Modal -->
				</div>
				<div class=" element--1--home element--1--banner5">
					<a class="dot-banner-animation">
						<div class="dot-banner--tooltip">Function not used in this exhibition</div>
					</a>
				</div>
				<div class=" element--1--home element--1--banner6">
					<a href="<?php echo bloginfo('url').'/floorlobby'; ?>" title="Exhibition hall center">&nbsp; </a>
				</div>
				<div class=" element--1--home element--1--banner7">
					<a class="dot-banner-animation">
						<div class="dot-banner--tooltip">Function not used in this exhibition</div>
					</a>
				</div>
			</div><!-- end  .element--1 -->
		</div>
	</div>

</main><!-- end #main -->




<?php get_footer(); ?>
<script type="text/javascript">
window.addEventListener("load", function(event) {
	jQuery(".category__main1--list").hover(function(){
	    if(jQuery(this).hasClass("active")){
	      jQuery(this).removeClass("active");
	      jQuery(".category__main1--list ul").slideUp();
	    }
	    else{
	      jQuery(this).addClass("active");
	      jQuery(this).children(".category__main1--list ul").slideDown();
	    }
	});
	var height1 = jQuery(".category__main1--list ul").height();
	if(height1 >  490){
	  jQuery(".category__main1--list ul").addClass('fixmenu');
	}
	else{
	  jQuery(".category__main1--list ul").removeClass('fixmenu');
	}
	var p = document.getElementById("countdown");
	var timeEnd = new Date("Oct 28,2020 09:00:00").getTime();
	var self = this;
	var countDown = setInterval(run,1000);
	function run(){

		var now = new Date().getTime();
		var timeRest = timeEnd - now;
		var day = Math.floor(timeRest/(1000*60*60*24));
		var hours = Math.floor(timeRest%(1000*60*60*24)/(1000*60*60));
		var minute = Math.floor(timeRest%(1000*60*60)/(1000*60));
		var sec = Math.floor(timeRest%(1000*60)/(1000));
		// debugger;
		p.innerHTML = '<ul><li><i>'+day+'</i><span>Days</span></li><li><i>'+hours+'</i><span>Hours</span></li><li><i>' + minute + '</i><span>Minute</span></li><li><i>' + sec +'</i><span>Seconds</span></li></ul>';
			if(timeRest <= 0){
			stopFunction();
		}
	}

	function stopFunction() {
		clearInterval(countDown);
		p.innerHTML = "";
	}


});


</script>


