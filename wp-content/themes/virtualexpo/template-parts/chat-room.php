<?php

/** * Template Name: Chat Room */
the_post();
global $current_user;
wp_get_current_user();
?>
<html <?php language_attributes(); ?> class="form-full-page-html">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>

<body>
  <div id="nw-chat-master" class="room-chat"></div>

  <!-- <iframe src="https://dashboard.tawk.to/" frameborder="0"></iframe> -->
  <div class="room-chat-button">
    <nav id="nav-ft" class="nav-ft">
      <?php if (have_rows('menu_chat_room', 'option')) : ?>
        <ul>
          <?php while (have_rows('menu_chat_room', 'option')) : the_row(); ?>
            <li>
              <a <?php $link = get_sub_field('link');
                  if (get_sub_field('link')) {
                  ?> href="<?php echo $link; ?>" <?php
                                                }
                                                  ?> <?php the_sub_field('more_tag'); ?>>
                <img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>">
                <?php the_sub_field('title'); ?>
              </a>
            </li>
          <?php endwhile; ?>
        </ul>
      <?php endif; ?>
    </nav>
  </div>
  </div>
</body>
<?php wp_footer(); ?>


<?php
$hasChat = !is_user_logged_in();
if (!$hasChat) {
  $roleUser = $current_user->roles[0];
  $hasChat = !($roleUser == "subscriber" or $roleUser == "um_visitor");
}
if ($hasChat) :
?>
  <!-- Load javascript codes -->
  <script src="<?=get_template_directory_uri()?>/js/socket.io.min.js"></script>
  <script src="<?=get_template_directory_uri()?>/js/exhibition-chat.js"></script>

  <script type="text/javascript">
    /**
     * @description
     * Below script is used for the messaging transportation between socket server and client
     * In order to communicate among visitors and exhibition telesales or other as well
     * Please contact writer or knowledger before modifing
     * 
     */
    // <?=$roleUser?>

    var mode = 'SUBSCRIBER_PAGE';
    var username = '<?= $current_user->ID ?>';
    var name = '<?= $current_user->display_name ?>' || username;
    ((mode, skOptions, credential) => new Exc(mode, skOptions, credential))(
      mode, { host: window.location.hostname }, { username, name }
    );
  </script>
<?php endif; ?>

</html>