<?php /** * Template Name: Form full page */ 
the_post();
?>
<html <?php language_attributes(); ?> class="form-full-page-html">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<div id="form-full-page">
	<?php the_content(); ?>
</div>

<?php wp_footer(); ?>
</html>
