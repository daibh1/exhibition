<?php
    /**
     * Display config Theme Cargolink
     */

    function theme_settings_page(){
        ?>
      <div id="wrap_admin" class="wrap_admin">
          <?php
              settings_fields("section_theme_config");
              do_settings_sections("theme-options-config");
          ?>
        <button onclick="showUploadBackground()">Upload background</button>
        <button id="saveJson" width="20px" height="10px">Update</button>
        <button id="clickAddElement">Create element</button>
        <script>
            function closePopup(object) {
                if(object === "background"){

                    jQuery("#wrap-show-background").removeClass('show');
                } else if(object === "edit"){
                    jQuery("#confirm-edit").removeClass("show");
                }
                else{
                    // logo
                    jQuery('#logoCompany').val('');
                    jQuery('#uploadLogo img').remove();
                    jQuery('#remove-image').removeClass('show');
                    jQuery('#btnUpLoadLogo').addClass('show');

                    // logo big
                    jQuery('#uploadLogoBig img').remove();
                    jQuery('#remove-imageBig').removeClass('show');
                    jQuery('#btnuploadLogoBig').addClass('show');
                    jQuery('#logoCompanyBig').val('');

                    jQuery('#description-company').val('');
                    jQuery('#company-name').val('');
                    jQuery('#link-company').val('');
                    jQuery("#wrap-show").removeClass('show');
                }
            }
            function uploadLogo(object ,event) {
                let mediaUploader;
                event.preventDefault();
                if (mediaUploader) {
                    mediaUploader.open();
                    return;
                }
                mediaUploader = wp.media.frames.file_frame = wp.media({
                    title: 'Choose Image',
                    button: {
                        text: 'Choose Image'
                    }, multiple: false });
                mediaUploader.on('select', function() {
                    let attachment = mediaUploader.state().get('selection').first().toJSON();
                    let linkLogo = attachment.url;
                    let img = '<img src="'+ linkLogo +'" height="60px">'
                    if(object == 'logo'){
                        if(linkLogo){
                            jQuery('#uploadLogo').append(img);
                            jQuery('#logoCompany').val(linkLogo);
                            jQuery('.uploadLogoClass .wpcf7-not-valid-tip').remove();
                            jQuery('#btnUpLoadLogo').removeClass('show');
                            jQuery('#remove-image').addClass('show');
                        } else {
                            jQuery('#remove-image').removeClass('show');
                            jQuery('#btnUpLoadLogo').addClass('show');
                        }
                    } else if (object == 'logoBig'){
                        if(linkLogo){
                            jQuery('#uploadLogoBig').append(img);
                            jQuery('#logoCompanyBig').val(linkLogo);
                            jQuery('.uploadLogoBig .wpcf7-not-valid-tip').remove();
                            jQuery('#btnuploadLogoBig').removeClass('show');
                            jQuery('#remove-imageBig').addClass('show');
                        } else {
                            jQuery('#remove-imageBig').removeClass('show');
                            jQuery('#btnuploadLogoBig').addClass('show');
                        }
                    } else if(object == 'background'){
                        jQuery('#uploadBackground').append(img);
                        jQuery('#urlBackground').val(linkLogo);
                    }
                });
                mediaUploader.open();
            }
            function removeImg(object) {
                if (object === "logo")
                {
                    jQuery('#logoCompany').val('');
                    jQuery("#uploadLogo img").remove();
                    jQuery('#remove-image').removeClass('show');
                    jQuery('#btnUpLoadLogo').addClass('show');
                }
                if (object === "logoBig")
                {
                    jQuery('#logoCompanyBig').val('');
                    jQuery("#uploadLogoBig img").remove();
                    jQuery('#remove-imageBig').removeClass('show');
                    jQuery('#btnuploadLogoBig').addClass('show');
                }

            }
            function showUploadBackground() {
                jQuery("#wrap-show-background").addClass('show');
            }
        </script>
      </div>
        <?php
    }
    function add_pdf_button() {
        global $post;
        $urlSetting = get_site_url().'/wp-admin/admin.php?page=theme-panel';
        $genre_url = add_query_arg('id',$post->ID, $urlSetting);
        echo '<a href="'.$genre_url.'" id="add-pdf" class="button" title=""><span class="wp-media-buttons-icon"></span>Add Design Page</a>';
    }
    add_action('media_buttons', 'add_pdf_button', 14);
    function exhibition_update_json(WP_REST_Request $request){
        $idPage = $_GET['id'];
        $dataJson = $request->get_json_params();
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'].'/exhibition/'.$idPage.'/';
        $pathJson = $upload_dir.'dataConfig.json';
        if (!file_exists($upload_dir)) {
            wp_mkdir_p($upload_dir);
        }
        file_put_contents($pathJson, json_encode($dataJson, TRUE));
        $response = 'Update Successful';
        return rest_ensure_response( $response );
    }
    function exhibition_create_json(WP_REST_Request $request) {
        $dataJson = $request->get_json_params();
        $nameCompany = $dataJson[0]["nameCompany"];
        $logoCompany = $dataJson[0]["urlLogo"];
        $linkCompany = $dataJson[0]["linkCompany"];
        $logoCompanyBig = $dataJson[0]["urlLogoBig"];
        $status = "";
        $message = "";
        $invalid_fields = array();
        $flag = true;
        if ($nameCompany == "") {
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.company-name',
                'message' => 'This field is require',
                'idref' => '',
            );
            $message = "One or more fields have an error. Please check and try again.";
            $flag = false;
        }
        if($linkCompany == ""){
            $message = "One or more fields have an error. Please check and try again.";
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.link-company',
                'message' => 'This field is require',
                'idref' => '',
            );
            $flag = false;
        }
        if($logoCompany == ""){
            $message = "One or more fields have an error. Please check and try again.";
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.uploadLogoClass',
                'message' => 'This field is require',
                'idref' => 'image',
            );
            $flag = false;
        }
        if($logoCompanyBig == ""){
            $message = "One or more fields have an error. Please check and try again.";
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.uploadLogoBig',
                'message' => 'This field is require',
                'idref' => 'image',
            );
            $flag = false;
        }
        if($flag){
            $message = "successfull";
            $status = "successfull";
        }
        $response = array(
            'status' => $status,
            'message' => $message,
        );
        if ( 'validation_failed' == $status ) {
            $response['invalid_fields'] = $invalid_fields;
        }
        return rest_ensure_response( $response );
    }
    function exhibition_get_json(WP_REST_Request $request){
        $idPage = $_GET['idPage'];
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'].'/exhibition/'.$idPage.'/';
        $pathJson = $upload_dir.'dataConfig.json';
        $response = '';
        if (file_exists($pathJson)) {
            $data = file_get_contents($pathJson);
            $response = json_decode($data, true);
        } else {
            $response = '';
        }

        return rest_ensure_response( $response );
    }
    function exhibition_get_background_json(WP_REST_Request $request){
        $idPage = $_GET['idPage'];
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'].'/exhibition/'.$idPage.'/';
        $pathJson = $upload_dir.'backgroundJson.json';
        $response = '';
        if (file_exists($pathJson)) {
            $data = file_get_contents($pathJson);
            $response = json_decode($data, true);
        } else {
            $response = '';
        }

        return rest_ensure_response( $response );
    }
    function exhibition_delete_json(WP_REST_Request $request){
        $upload = wp_upload_dir();
        $idPage = $_GET['idPage'];
        $idElement = substr(substr($request->get_body(), 0, -1), 1);;
        $upload_dir = $upload['basedir'].'/exhibition/'.$idPage.'/';
        $pathJson = $upload_dir.'dataConfig.json';
        $data = file_get_contents($pathJson ,true);
        $jsonArr = json_decode( $data, TRUE);
        $arr_index = array();
        foreach ($jsonArr as $key => $value) {
            if ($value['id'] == $idElement) {
                var_dump($key);
                $arr_index[] = $key;
            }
        }
        foreach ($arr_index as $i) {
            unset($jsonArr[$i]);
        }
        $jsonArr = array_values($jsonArr);
        file_put_contents($pathJson, json_encode($jsonArr));
    }
    function exhibition_crate_background_json(WP_REST_Request $request){
        $dataJson = $request->get_json_params();
        $idPage = $_GET['id'];
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'].'/exhibition/'.$idPage.'/';
        $pathJson = $upload_dir.'backgroundJson.json';
        $widthBackground = $dataJson[0]["width"];
        $heightBackground = $dataJson[0]["height"];
        $urlBackground = $dataJson[0]["background"];
        $status = "";
        $message = "";
        $invalid_fields = array();
        $flag = true;
        if ($widthBackground == "") {
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.width-background',
                'message' => 'This field is require',
                'idref' => '',
            );
            $message = "One or more fields have an error. Please check and try again.";
            $flag = false;
        }
        if($heightBackground == ""){
            $message = "One or more fields have an error. Please check and try again.";
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.height-background',
                'message' => 'This field is require',
                'idref' => '',
            );
            $flag = false;
        }
        if($urlBackground == ""){
            $message = "One or more fields have an error. Please check and try again.";
            $status = "validation_failed";
            $invalid_fields[] = array(
                'into' => 'span.wpcf7-form-control-wrap.urlBackground',
                'message' => 'This field is require',
                'idref' => 'image',
            );
            $flag = false;
        }
        if($flag){
            $message = "successfull";
            $status = "successfull";
            if (!file_exists($upload_dir)) {
                wp_mkdir_p($upload_dir);
            }
            file_put_contents($pathJson, json_encode($dataJson, TRUE));
        }
        $response = array(
            'status' => $status,
            'message' => $message,
        );
        if ( 'validation_failed' == $status ) {
            $response['invalid_fields'] = $invalid_fields;
        }

        return rest_ensure_response( $response );
    }
    function get_list_exhibition(WP_REST_Request $request){
      $sqlCompany = "(select user_id, meta_value from virtudb_usermeta where meta_key = 'company_name'
                  and user_id IN (
                 select user_id from virtudb_usermeta
                where meta_key = 'virtudb_capabilities' and not meta_value like '%um_exhibitor-extra%')) a";
      $sqlField = "(select user_id, meta_value, meta_key from virtudb_usermeta
                where meta_key = 'country' and user_id IN (
                 select user_id from virtudb_usermeta
                where meta_key = 'virtudb_capabilities' and meta_value like '%um_exhibitor%')) b";
      $sqlLinkBoth = "(select user_id, meta_value from virtudb_usermeta
                where meta_key = 'link_booth'
               ) c";
      $sql = "select distinct a.meta_value as company_name, b.meta_value as country, c.meta_value as link_booth
              from virtudb_usermeta
              INNER JOIN " . $sqlCompany .
              " ON virtudb_usermeta.user_id = a.user_id" .
              " INNER JOIN " . $sqlField .
              " ON virtudb_usermeta.user_id = b.user_id" .
              " INNER JOIN " . $sqlLinkBoth .
              " ON virtudb_usermeta.user_id = c.user_id";
      $data = $GLOBALS['wpdb']->get_results($sql);
      return rest_ensure_response( $data );
    }
    function get_list_exhibition_extra(WP_REST_Request $request){
        $sqlCompany = "(select user_id, um_value from virtudb_um_metadata where um_key = 'company_name'
                    and user_id IN (
                    select user_id from virtudb_um_metadata
                    where um_key = 'virtudb_capabilities' and um_value like '%um_exhibitor-extra%')) a";
        $sqlField = "(select user_id, meta_value from virtudb_usermeta
                    where meta_key = 'country') b";
        $sqlLinkBoth = "(select user_id, um_value from virtudb_um_metadata
                    where um_key = 'link_booth'
                ) c";
        $sql = "select distinct a.um_value as company_name, b.meta_value as country, c.um_value as link_booth
                from virtudb_um_metadata
                INNER JOIN " . $sqlCompany .
            " ON virtudb_um_metadata.user_id = a.user_id" .
            " INNER JOIN " . $sqlField .
            " ON virtudb_um_metadata.user_id = b.user_id" .
            " INNER JOIN " . $sqlLinkBoth .
            " ON virtudb_um_metadata.user_id = c.user_id";
        $data = $GLOBALS['wpdb']->get_results($sql);
        return rest_ensure_response( $data );
    }
    function get_list_listenned_exhibition_old(WP_REST_Request $request) {
        $userId = esc_sql($_GET['userId']);
        $field_value = sprintf( '^%1$s$|s:%2$u:"%1$s";', $userId, strlen( $userId ) );

        $posts = get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'post',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'	 	=> 'listener',
                    'value'	  	=> $field_value,
                    'compare' 	=> 'REGEXP',
                )
            ),
        ));
        return rest_ensure_response( $posts );
    }
    function get_list_listenned_exhibition(WP_REST_Request $request) {
        $userId = esc_sql($_GET['userId']);
        $exhibitions = get_user_meta($userId, 'exhibition', true);
        $posts = get_posts(array(
            'post__in'		=> (array) $exhibitions,
        ));
        return rest_ensure_response( $posts );
    }
    function get_list_conversion_of_exhibition(WP_REST_Request $request) {
        $userId = esc_sql($_GET['userId']);
        $exhibitions = get_user_meta($userId, 'exhibition', true);
        $posts = get_posts(array(
            'post__in'		=> (array) $exhibitions,
        ));
        if ( $posts[0] ) {
            $exhibitionId = esc_sql($posts[0]->ID);
            $page = empty($_GET['page']) ? 0 : (int)$_GET['page'];
            $page = $page < 0 ? 0 : $page;
            $size = empty($_GET['size']) ? 10 : (int)$_GET['size'];
            $size = $size < 1 ? 10 : $size;
            $offset = $page * $size;
            $sql = "SELECT count(*) AS `count` FROM `virtudb_conversion` WHERE `exhibition_id` = '".$exhibitionId."'";
            $count = $GLOBALS['wpdb']->get_var($sql);

            $fetchedField = "SELECT SQL_CALC_FOUND_ROWS `virtudb_conversion`.`id`, `exhibition_id` AS `exhibition`, `user_id` AS `username`, `display_name` AS `name`";
            $sqlJoinUser = "INNER JOIN `virtudb_users` on `virtudb_users`.`id` = `user_id`";
            $sql = $fetchedField." FROM `virtudb_conversion` ".$sqlJoinUser." WHERE `exhibition_id` = '".$exhibitionId."' LIMIT ".$offset.", ".$size."";
            $data = $GLOBALS['wpdb']->get_results($sql);
            return rest_ensure_response( array('data' => $data, 'count' => $count, 'page' => $page, 'size' => $size) );
        }
        // $exhibitionId = esc_sql($_GET['exhibitionId']);
        return rest_ensure_response(array('data' => array(), 'count' => 0, 'page' => 0, 'size' => 0));
    }

    function get_list_attendee(WP_REST_Request $request){
        $sql =  "Select meta.company_name, meta.demand, u.user_email, u.display_name, um.meta_value FROM
              (select distinct a.user_id,a.um_value as company_name, b.um_value as demand
                            from virtudb_um_metadata
                            INNER JOIN (select user_id, um_value from virtudb_um_metadata where um_key = 'company_name')
              a ON virtudb_um_metadata.user_id = a.user_id INNER JOIN (select user_id, um_value from virtudb_um_metadata
                              where um_key = 'demand') b ON virtudb_um_metadata.user_id = b.user_id) meta
              INNER JOIN virtudb_users u ON meta.user_id = u.id
              INNER JOIN (SELECT user_id, meta_value FROm virtudb_usermeta where meta_key = 'phone_number') um ON meta.user_id = um.user_id
            INNER JOIN (SELECT user_id FROM virtudb_um_metadata WHERE um_key = 'attendee_or_not') attendee ON u.id = attendee.user_id WHERE meta.demand <> ''";
      $data = $GLOBALS['wpdb']->get_results($sql);
      return rest_ensure_response( $data );
    }
    function get_session_temp(WP_REST_Request $request)
    {
        $sql = "select userId, products from virtudb_session_temp where userId = '".$_GET['userId']."'";
        $data = $GLOBALS['wpdb']->get_results($sql);
        return rest_ensure_response( $data );
    }
    function insert_session_temp(WP_REST_Request $request)
    {
        global $wpdb;
        $data = $request->get_json_params();
        $product = json_encode($data['product']);

        $result = $wpdb->insert(
            'virtudb_session_temp',
            array(
                'userId' => $data['userId'],
                'products' => $product
            )
        );
        if ($result)
        {
            return true;
        }
        return false;

    }
    function update_session_temp(WP_REST_Request $request)
    {
        global $wpdb;
        $data = $request->get_json_params();
        $product = json_encode($data['product']);
        $result = $wpdb->query($wpdb->prepare("update virtudb_session_temp set products = '".$product."' where userId = '".$data['userId']."'"));
        if ($result)
        {
            return true;
        }
        return false;

    }

/**
     * This function is where we register our routes for our example endpoint.
     */
    function prefix_register_example_routes() {

        // register_rest_route() handles more arguments but we are going to stick to the basics for now.
        register_rest_route( 'exhibition/v1', '/createElement', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_create_json',
        ) );
        register_rest_route( 'exhibition/v1', '/update', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_update_json',
        ) );
        register_rest_route( 'exhibition/v1', '/delete', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_delete_json',
        ) );
        register_rest_route( 'exhibition/v1', '/getJson', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_get_json',
        ) );
        register_rest_route( 'exhibition/v1', '/createBackground', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_crate_background_json',
        ) );
        register_rest_route( 'exhibition/v1', '/getBackground', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'exhibition_get_background_json',
        ) );
        register_rest_route( 'exhibition/v1', '/getListExhibition', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_exhibition',
        ) );
        register_rest_route( 'exhibition/v1', '/getListExhibitionExtra', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_exhibition_extra',
        ) );
        register_rest_route( 'exhibition/v1', '/getListListennedExhibition', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_listenned_exhibition',
        ) );
        register_rest_route( 'exhibition/v1', '/getListListennedExhibition_old', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_listenned_exhibition_old',
        ) );
        register_rest_route( 'exhibition/v1', '/getListConversionOfExhibition', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_conversion_of_exhibition',
        ) );
        register_rest_route( 'exhibition/v1', '/getListAttendee', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_list_attendee',
        ) );
        register_rest_route( 'exhibition/v1', '/getSessionTemp', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::READABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'get_session_temp',
        ) );
        register_rest_route( 'exhibition/v1', '/updateSessionTemp', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'update_session_temp',
        ) );
        register_rest_route( 'exhibition/v1', '/insertSessionTemp', array(
            // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
            'methods'  => WP_REST_Server::CREATABLE,
            // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
            'callback' => 'insert_session_temp',
        ) );

    }
    add_action( 'rest_api_init', 'prefix_register_example_routes' );
    function add_form_info_element() { ?>
      <div id="wrap-show" class="modal fade">
        <div role="form" class="wpcf7">
          <div class="screen-reader-response" role="alert" aria-live="polite"></div>
          <form>
            <div style="display: none;">
              <input type="hidden" name="_wpcf7" value="34">
              <input type="hidden" name="_wpcf7_version" value="5.2.1">
              <input type="hidden" name="_wpcf7_locale" value="en_US">
              <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f34-o1">
              <input type="hidden" name="_wpcf7_container_post" value="0">
              <input type="hidden" name="_wpcf7_posted_data_hash" value="">
              <input type="hidden" id="idElement" name="link-company" value="" size="40" class="wpcf7-text" aria-invalid="false" />
            </div>
            <div class="information">
              <label> Company Name <span class="require">*</span></label><br>
              <span class="wpcf7-form-control-wrap company-name">
            <input type="text" id="company-name" name="company-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
          </span><br>
              <label class="linkCompany">Link <span class="require">*</span></label><br>
              <span class="wpcf7-form-control-wrap link-company">
            <input type="text" id="link-company" name="link-company" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
          </span>
              <br>
              <label class="linkCompany">Description</label><br>
              <span class="wpcf7-form-control-wrap description-company">
            <textarea id="description-company" name="description-company" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
          </span>
              <br>
              <label class="logoCompany"> Company Logo <span class="require">*</span></label><p></p>
              <div id="uploadLogo">
                <button id="btnUpLoadLogo" class="show" type="button" onclick="uploadLogo('logo', event)">upload</button><br>
                <span class="wpcf7-form-control-wrap uploadLogoClass">
              <input type="hidden" id="logoCompany" name="uploadLogoClass" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="image" aria-required="true" aria-invalid="false" />
            </span><br>
                <button id="remove-image" type="button" onclick="removeImg('logo')">Delete</button>
              </div>
                <!--   start  logo company big       -->
                <br>
                <label class="logoCompanyBig"> Company Logo Big <span class="require">*</span></label><p></p>
                <div id="uploadLogoBig">
                    <button id="btnuploadLogoBig" class="show" type="button" onclick="uploadLogo('logoBig', event)">upload</button><br>
                    <span class="wpcf7-form-control-wrap uploadLogoBig">
                        <input type="hidden" id="logoCompanyBig" name="uploadLogoBig" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="imageBig" aria-required="true" aria-invalid="false" />
                    </span>
                    <br>
                    <button id="remove-imageBig" type="button" onclick="removeImg('logoBig')">Delete</button>
                </div>
                <!--   end  logo company small       -->

            </div>
            <div class="wrap-action">
              <button type="button" onclick="closePopup()" class="close">Close</button>
              <button id="sendElement" type="button" class="close">Send</button>
            </div>
          </form>
        </div>
      </div>
    <?php }
    add_action('admin_footer', 'add_form_info_element');

    function add_form_info_background() { ?>
      <div id="wrap-show-background" class="modal fade">
        <div role="form" class="wpcf7">
          <div class="screen-reader-response" role="alert" aria-live="polite"></div>
          <div class="information">
            <label> Width <span class="require">*</span></label><br>
            <span class="wpcf7-form-control-wrap width-background">
            <input type="text" id="width-background" name="width-background" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
          </span><br>
            <label class="linkCompany">Height <span class="require">*</span></label><br>
            <span class="wpcf7-form-control-wrap height-background">
            <input type="text" id="height-background" name="height-background" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
          </span>
            <br>
            <label class="logoCompany"> Upload background <span class="require">*</span></label><p></p>
            <div id="uploadBackground">
              <button onclick="uploadLogo('background', event)">Upload</button><br>
              <span class="wpcf7-form-control-wrap urlBackground">
              <input type="hidden" id="urlBackground" name="urlBackground" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
            </span><br>
              <button id="remove-image" type="button" onclick="removeImg()">Delete</button>
            </div>
          </div>
          <div class="wrap-action">
            <button onclick="closePopup('background')">Close</button>
            <button id="submitBackground">Submit</button>
          </div>
        </div>
      </div>
    <?php }
    add_action('admin_footer', 'add_form_info_background');

    function add_form_edit_background() { ?>
      <div id="confirm-edit" class="confirm modal">
        <div class="wpcf7">
          <button id="edit">edit</button>
          <button id="delete">delete</button>
          <button onclick="closePopup('edit')">close</button>
        </div>
      </div>
    <?php }
    add_action('admin_footer', 'add_form_edit_background');
    function add_theme_menu_item()
    {
        add_menu_page("Theme Setting", "Theme Setting", "manage_options", "theme-panel", "theme_settings_page", null, 99);
    }
    add_action("admin_menu", "add_theme_menu_item");
    function add_menu_admin_bar_link() {
        global $wp_admin_bar;
        $args = array('id' => 'menu_id','title' => __('Quản lí khác'),'href' => __('../templates/options-general.php'),);
        $wp_admin_bar->add_menu( $args );
    }
    add_action('admin_bar_menu', 'add_menu_admin_bar_link',81);

    function media_uploader_enqueue(){
        wp_enqueue_media();
        wp_register_script('media-uploader', get_stylesheet_directory_uri() . '/js/d3.js' , array('jquery'));
        wp_register_script('contact-form', get_stylesheet_directory_uri() . '/js/contact-form.js' , array('jquery'),'',true);
        wp_enqueue_script('media-uploader');
        wp_enqueue_script('contact-form');
        wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
    }

    add_action('admin_enqueue_scripts', 'media_uploader_enqueue');
?>
