<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Virtualexpo
 */

use RankMath\Paper\Date;

?>
<?php global $current_user; wp_get_current_user(); 
?>

<nav id="nav-ft" class="nav-ft">
  <?php if( have_rows('menu', 'option') ): ?>
      <ul>
      <?php while( have_rows('menu', 'option') ): the_row(); ?>
          <li>
        <a
        <?php $link = get_sub_field('link');
          if (get_sub_field('link')) {
            ?>
            href="<?php echo $link; ?>"
            <?php
          }
        ?>
        <?php the_sub_field('more_tag'); ?>>
          <img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>">
          <?php the_sub_field('title'); ?>
        </a>
      </li>
      <?php endwhile; ?>
      </ul>
  <?php endif; ?>
  <div style="
    text-align: center;
    padding-top: 6px;
    font-size: 10px;
    color: #999;
">Application developed by <a target="_blank" href="https://aegmedia.vn/">AEG Media</a> and <a target="_blank" href="https://globalexpo.com.vn/">Globalexpo</a></div>
</nav>
<div class="menu-navigation">
<?php
  wp_nav_menu( array(
    'theme_location' => 'primary',
    'container' => false,   
    'menu_id' => '',
    'menu_class' => '',
    )
  );
?>
</div>
<!-- <footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-9 footer--left">
        <?php
          wp_nav_menu( array(
          'theme_location' => 'primary',
          'container' => false,
          'menu_id' => '',
          'menu_class' => '',
          )
          );
        ?>
      </div>
      <div class="col-12 col-md-3 footer--right text-right">VR-GlobalExpo 2020</div>
    </div>
  </div>
</footer>  -->
<!-- end #footer -->
</div><!-- end #body -->
<?php wp_footer(); ?>
</body>
</html>

<!-- Modal -->
<div class="modal fade" id="popup-egistration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registration Visitor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="post" class="content-primer">
          <iframe style="height: 60vh;width: 100%" src="<?php echo get_site_url(); ?>/register/" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ENd Modal -->
<!-- Modal Exhibition Name -->
<div class="modal fade" id="exhibitionCompany" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Exhibitors</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php
      $userHaveLogin = do_shortcode('[um_loggedin]');
      if($userHaveLogin != ""):
        ?>
        <div >
          <div class="text-center"><a href="<?php echo get_site_url().'/login/'; ?>" style="color:red;">Please login</a></div>
        </div>
      <?php
      else: ?>
      <div id="wrapTableCompanyName" class="content-primer">
      </div>
      <?php endif ?>
      </div>
    </div>
  </div>
</div>
<!-- ENd Modal -->
<!-- Modal briefcase Name -->
<div class="modal fade" id="briefcase" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Briefcase</h5>
                <button type="button" id="closeModalBriefCase" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                $userHaveLogin = do_shortcode('[um_loggedin]');
                if($userHaveLogin != ""):
                    ?>
                    <div >
                        <div class="text-center"><a href="<?php echo get_site_url().'/login/'; ?>" style="color:red;">Please login</a></div>
                    </div>
                <?php
                else: ?>
                    <div id="wrapTableBriefCase" class="content-primer">
                    </div>
                <?php endif ?>
                <div class="alert alert-success alert-dismissible fade" role="alert" id="buttonAlertDelete">
                    <!-- <strong>Document is added successfully!</strong>-->
                    <strong></strong>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ENd Modal -->

<!-- Modal Exhibition Extra Name -->
<div class="modal fade" id="exhibitionExtra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exhibitors Extra</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                $userHaveLogin = do_shortcode('[um_loggedin]');
                if($userHaveLogin != ""):
                    ?>
                    <div >
                        <div class="text-center"><a href="<?php echo get_site_url().'/login/'; ?>" style="color:red;">Please login</a></div>
                    </div>
                <?php
                else: ?>
                    <iframe src="https://drive.google.com/file/d/1cUw9ZS33x1wcFpUCdjXBU82HEa7lcPAI/preview" style="height: 65vh;"></iframe>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<!-- ENd Modal -->

<script>
    let count = 0;
    $("#exhibitors-click").on('click', function () {
        if(count < 1){
            $.ajax({
                type: 'GET',
                url: '<?php echo get_site_url() .'/wp-json/exhibition/v1/getListExhibition'?>',
                dataType: 'json',
                processData: false,
                contentType: false
            }).done(function (data, status, xhr) {
                var table_html = [];
                var thTable_html_company = [
                    {
                        name: "Company Names",
                        class: "exhibitor"
                    },
                    {
                        name: "Country",
                        class: "country"
                    },
                    {
                        name: "View Stalls",
                        class: "view"
                    }
                ];
                var item = data;
                var itemthead = thTable_html_company;
                if(item.length > 0){
                    table_html.push('<table id="tableCompanyName" class="display">');
                    table_html.push('<thead>');
                    table_html.push('<tr class="tableRow">');
                    for (var i = 0; i < itemthead.length; i++) {
                        table_html.push('<th class="' + itemthead[i].class + '">' + itemthead[i].name);
                        table_html.push('</th>');
                    }
                    table_html.push('</tr>');
                    table_html.push('</thead>');
                    table_html.push('<tbody>');
                    for (var i=0; i < item.length;  i++) {
                        table_html.push('<tr>');
                        table_html.push('<td class="exhibitor"><span>' + item[i].company_name + '</span>');
                        table_html.push('</td>');
                        table_html.push('<td class="interest"><span>' + item[i].country + '</span>');
                        table_html.push('</td>');
                        table_html.push('<td class="view viewDetails"><button><a alt="' + item[i].company_name + '" href="' + item[i].link_booth + '">View Details</a></button>');
                        table_html.push('</td>');
                        table_html.push('</tr>');
                    }
                    table_html.push('</tbody>');
                    table_html.push(' </table>');
                    jQuery('#wrapTableCompanyName').prepend(table_html.join(''));
                    if(data){
                        var datatable = jQuery('#tableCompanyName').DataTable(
                            {
                              paging:   false,
                                info:     false,
                                searching: true,
                                select: false,
                                ordering: false,
                                language: {
                                    "info": "_START_ - _END_ Trên _TOTAL_",
                                }
                            }
                        );
                    }
                } else {
                    table_html.push('<span class ="no-event">No Exhibition</span>')
                    jQuery('#table-bang-xep-hang').prepend(table_html);
                }
            }).fail(function (xhr, status, error) {
                console.debug(error);
            });
        }
        count ++;
    });

    // let countExtra = 0;
    // $("#exhibitorsExtra-click").on('click', function () {
    //     if(countExtra < 1){
    //         $.ajax({
    //             type: 'GET',
    //             url: '<?php // echo get_site_url() .'/wp-json/exhibition/v1/getListExhibitionExtra'?>',
    //             dataType: 'json',
    //             processData: false,
    //             contentType: false
    //         }).done(function (data, status, xhr) {
    //             var table_html = [];
    //             var thTable_html_company = [
    //                 {
    //                     name: "Company Names",
    //                     class: "exhibitor"
    //                 },
    //                 {
    //                     name: "Country",
    //                     class: "country"
    //                 },
    //                 {
    //                     name: "View Stalls",
    //                     class: "view"
    //                 }
    //             ];
    //             var item = data;
    //             var itemthead = thTable_html_company;
    //             if(item.length > 0){
    //                 table_html.push('<table id="tableExhibitionExtra" class="display">');
    //                 table_html.push('<thead>');
    //                 table_html.push('<tr class="tableRow">');
    //                 for (var i = 0; i < itemthead.length; i++) {
    //                     table_html.push('<th class="' + itemthead[i].class + '">' + itemthead[i].name);
    //                     table_html.push('</th>');
    //                 }
    //                 table_html.push('</tr>');
    //                 table_html.push('</thead>');
    //                 table_html.push('<tbody>');
    //                 for (var i=0; i < item.length;  i++) {
    //                     table_html.push('<tr>');
    //                     table_html.push('<td class="exhibitor"><span>' + item[i].company_name + '</span>');
    //                     table_html.push('</td>');
    //                     table_html.push('<td class="country"><span>' + item[i].country + '</span>');
    //                     table_html.push('</td>');
    //                     table_html.push('<td class="view viewDetails"><button><a alt="' + item[i].company_name + '" href="' + item[i].link_booth + '">View Details</a></button>');
    //                     table_html.push('</td>');
    //                     table_html.push('</tr>');
    //                 }
    //                 table_html.push('</tbody>');
    //                 table_html.push(' </table>');
    //                 jQuery('#wrapTableExhibitionExtra').prepend(table_html.join(''));
    //                 if(data){
    //                     var datatable = jQuery('#tableExhibitionExtra').DataTable(
    //                         {
    //                             paging:   false,
    //                             info:     false,
    //                             searching: true,
    //                             select: false,
    //                             ordering: false,
    //                             language: {
    //                                 "info": "_START_ - _END_ Trên _TOTAL_",
    //                             }
    //                         }
    //                     );
    //                 }
    //             } else {
    //                 table_html.push('<span class ="no-event">No Exhibition Extra</span>')
    //                 jQuery('#table-bang-xep-hang').prepend(table_html);
    //             }
    //         }).fail(function (xhr, status, error) {
    //             console.debug(error);
    //         });
    //     }
    //     countExtra ++;
    // });
</script>
<!-- Modal -->
<div class="modal fade" id="popup-document-web" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Exhibition schedule</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe src="https://drive.google.com/file/d/<?php the_field('id_brochure','option'); ?>/preview" frameborder="0"></iframe>
      </div>
    </div>
  </div>
</div>
<!-- ENd Modal -->

<!-- Modal Exhibition Name -->
<div class="modal fade" id="attendee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Attendee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        if ( is_user_logged_in() ) : ?>
          <div id="wrapTableAttendee" class="content-primer">
          </div>
        <?php else : ?>
          <div >
            <div class="text-center"><a href="<?php echo get_site_url().'/login/'; ?>" style="color:red;">Please login</a></div>
          </div>
        <?php endif ?>
      </div>
    </div>
  </div>
</div>
<!-- ENd Modal -->
<script>

    let countAttendee = 0;
    $("#attendee-click").on('click', function () {
        if(countAttendee < 1){

            $.ajax({
                type: 'GET',
                url: '<?php echo get_site_url() .'/wp-json/exhibition/v1/getListAttendee'?>',
                dataType: 'json',
                processData: false,
                contentType: false
            }).done(function (dataAtendee, status, xhr) {
                var table_html = [];
                var thTable_html_company = [
                    {
                        name: "Company Names",
                        class: "companyName"
                    },
                    {
                        name: "Demand",
                        class: "demand"
                    },
                    {
                        name: "Information",
                        class: "companyEmail"
                    }
                ];
                var item = dataAtendee;
                var itemthead = thTable_html_company;
                if(item.length > 0){
                    table_html.push('<table id="tableAttendee" class="display table table-striped">');
                    table_html.push('<thead>');
                    table_html.push('<tr class="tableRow">');
                    for (var i = 0; i < itemthead.length; i++) {
                        table_html.push('<th class="' + itemthead[i].class + '">' + itemthead[i].name);
                        table_html.push('</th>');
                    }
                    table_html.push('</tr>');
                    table_html.push('</thead>');
                    table_html.push('<tbody>');
                    for (var i=0; i < item.length;  i++) {
                        table_html.push('<tr>');
                        table_html.push('<td class="companyName"><span>' + item[i].company_name + '</span>');
                        table_html.push('</td>');
                        table_html.push('<td class="demand"><span>' + item[i].demand + '</span>');
                        table_html.push('</td>');
                        table_html.push('<td class="companyEmail"><span class="ct-info">information</span><div class="content-info"><p>' + item[i].display_name + '</p>' +
                            '<p>' + '</p>' +
                            '<p>' + item[i].user_email + '</p>' +
                            '<p>' + '</p>' +
                            '<p>0' + item[i].meta_value + '</p>' +
                            '');
                        table_html.push('</div></td>');
                        table_html.push('</tr>');
                    }
                    table_html.push('</tbody>');
                    table_html.push(' </table>');
                    jQuery('#wrapTableAttendee').prepend(table_html.join(''));
                    if(dataAtendee){
                        var datatable = jQuery('#tableAttendee').DataTable(
                            {
                                paging:   false,
                                info:     false,
                                searching: true,
                                select: false,
                                autoWidth: true,
                                ordering: false,
                                language: {
                                  "info": "_START_ - _END_ Trên _TOTAL_",
                                }
                            }
                        );
                        jQuery('.ct-info').on('click', function () {
                            if(jQuery(this).hasClass("active")){
                                jQuery(this).removeClass("active");
                                jQuery(".content-info").slideUp();
                            }
                            else{
                                jQuery(".ct-info").removeClass("active");
                                jQuery(".content-info").slideUp(500);
                                jQuery(this).addClass("active");
                                jQuery(this).parent('.companyEmail').children(".content-info").slideDown();
                            }
                        } );
                    }
                } else {
                    table_html.push('<span class ="no-event">No Attendee</span>')
                    jQuery('#table-bang-xep-hang').prepend(table_html);
                }
            }).fail(function (xhr, status, error) {
                console.debug(error);
            });
        }
        countAttendee ++;


    });
</script>

<script type="text/javascript">


window.addEventListener("load", function (event) {


$('#date-booth').datetimepicker({
 minDate:0
});

});

</script>
<script type="text/javascript">
    function renderHTML(thComponent, objectData)
    {
        var table_html = [];
        var items = objectData;
        var itemthead = thComponent;
        table_html.push('<table id="tableBriefCase" class="display">');
        table_html.push('<thead>');
        table_html.push('<tr class="tableRow">');
        for (var i = 0; i < itemthead.length; i++) {
            table_html.push('<th class="' + itemthead[i].class + '" >' + itemthead[i].name);
            table_html.push('</th>');
        }
        table_html.push('</tr>');
        table_html.push('</thead>');
        table_html.push('<tbody>');
        for (var i=0; i < items.product.length;  i++) {
            table_html.push('<tr>');
            table_html.push('<td class="companyName"><a href="'+items.product[i].boothLink+'"><span>' + items.product[i].booth + '</span></a>');
            table_html.push('</td>');
            table_html.push('<td class="document-name"><span>' + items.product[i].documentName + '</span>');
            table_html.push('</td>');
            table_html.push('<td class="document-link">');
            table_html.push(`<a target="_blank" id="document-link" href="${items.product[i].documentLink}"><img width="58.33px" height="33px" src="<?php echo get_template_directory_uri(); ?>/images/download.svg" alt=""></a>`);
            table_html.push('</td>');
            table_html.push('<td class="action" id="action">' +
                '<button type="button" id="delete-document" class="delete">\n' +
                '<span aria-hidden="true">&times;</span>\n' +
                '</button>');
            table_html.push('</td>');
            table_html.push('</tr>');
        }
        table_html.push('</tbody>');
        table_html.push(' </table>');
        jQuery('#wrapTableBriefCase').prepend(table_html.join(''));
        if(items){
            var datatable = jQuery('#tableBriefCase').DataTable(
                {
                    paging:   false,
                    info:     false,
                    searching: true,
                    select: false,
                    ordering: false,
                    language: {
                        "info": "_START_ - _END_ Trên _TOTAL_",
                    }
                },
            );
        }else {
            table_html.push('<span class ="no-event">no products</span>')
            return jQuery('#wrapTableBriefCase').prepend(table_html.join(''));
        }
    }

    // briefcase-click start
    jQuery("#briefcase-click").on('click', function () {

        var table_html = [];
        var userID = <?php echo $current_user->ID ?>;
        var sessionCart = localStorage.getItem('addToCart');
        sessionCart = JSON.parse(sessionCart);
        var items = sessionCart;
        var thComponent = [
            {
                name: "Company Name",
                class: "companyName"
            },
            {
                name: "Document Name",
                class: "document-name"
            },
            {
                name: "Document Link",
                class: "document-link"
            },
            {
                name : "Action",
                class: "action"
            }
        ];
        if (sessionCart)
        {
            return renderHTML(thComponent, items)
        }
        else
        {
            table_html.push('<span class ="no-event">no products</span>');
            return jQuery('#wrapTableBriefCase').prepend(table_html.join(''));
        }

        $.ajax({
            type: 'GET',
            url: '<?php echo get_site_url() .'/wp-json/exhibition/v1/getSessionTemp?userId=' . $current_user->ID?>',
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function (data, startus, xhr) {

            if (data.length > 0)
            {
                var objectSessionTemp = {
                    userId : data[0].userId,
                    product : JSON.parse(data[0].products)
                }
                localStorage.addToCart = JSON.stringify(objectSessionTemp);

                return renderHTML(thComponent, objectSessionTemp)
            }
            else
            {
                table_html.push('<span class ="no-event">no products</span>');
                return jQuery('#wrapTableBriefCase').prepend(table_html.join(''));
            }
        })

    });
    // briefcase-click end
    $(function () {
        $('#briefcase').on('hidden.bs.modal', function () {
            jQuery('#tableBriefCase_wrapper').remove();
        });
    });

</script>

<?php
  global $wp_query;
  $loggedIn = is_user_logged_in();
  $post_id   = $wp_query->get_queried_object_id();
  $chat_mode = 'VISITOR';
  if ($loggedIn) {
    $roleUser = $current_user->roles[0];
    $chat_mode = ($roleUser == "subscriber" or $roleUser == "um_visitor") ? 'VISITOR' : 'SUBSCRIBER_POPUP';
  }
?>


<!-- Load javascript codes -->
<script src="<?=get_template_directory_uri()?>/js/socket.io.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/exhibition-chat.js"></script>

<script type="text/javascript">
  /**
   * @description
   * Below script is used for the messaging transportation between socket server and client
   * In order to communicate among visitors and exhibition telesales or other as well
   * Please contact writer or knowledger before modifing
   * 
   */
  // <?=$roleUser?>

  var mode = '<?=$chat_mode?>';
  var username = '<?= $current_user->ID == 0 ? 'Guest' . round(microtime(true)) : $current_user->ID ?>';
  var name = '<?= $current_user->display_name ?>' || username;
  var post = '<?=$post_id?>';
  ((mode, skOptions, credential, post) => new Exc(mode, skOptions, credential, post))(
    mode, { host: window.location.hostname }, { username, name }, post
  );

</script>

<?php the_field('code_footer','option') ?>